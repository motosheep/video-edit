package com.north.light.librxffmpeg.bean;

import java.io.Serializable;

/**
 * author:li
 * date:2022/3/22
 * desc:获取的视频信息
 */
public class FFmpegVideoInfo implements Serializable {

    private String creation_time;
    private String iformat_name;
    private String videostream_nb_frames;
//    private String com.android.manufacturer;
    private String protocol_blacklist;
    private String videostream_codecpar_bit_rate;
    private String videostream_codecpar_height;
    private String minor_version;
    private String videostream_sample_aspect_ratio_num;
    private String videostream_codec_time_base;
    private String audiostream_avcodocname;
    private String videostream_size;
    private String videostream_codecpar_width;
    private String videostream_codec_fourcc;
    private String videostream_profilestring;
    private String videostream_avg_frame_rate;
    private String videostream_avcodocname;
    private String videostream_sample_aspect_ratio_den;
//    private String com.android.version;
    private String audiostream_size;
    private String iformat_long_name;
    private String protocol_whitelist;
    private String videostream_duration;
    private String filesize;
    private String duration;
    private String bit_rate;
    private String max_picture_buffer;
    private String videostream_r_frame_rate;
    private String max_interleave_delta;
    private String audiostream_codecpar_channels;
    private String audiostream_profilestring;
//    private String com.android.model;
    private String videostream_time_base;
    private String compatible_brands;
    private String max_ts_probe;
    private String videostream_codecpar_bits_per_coded_sample;
    private String videostream_time_base_num;
    private String major_brand;
    private String pix_fmt_name;
    private String videostream_codecpar_codec_type;
    private String videostream_time_base_den;
    private String audiostream_codecpar_codec_type;
    private String url;
    private String videostream_codecpar_bits_per_raw_sample;
    private String display_aspect_ratio_den;
    private String audiostream_codecpar_bit_rate;
    private String audiostream_duration;
    private String display_aspect_ratio_num;
    private String audiostream_codec_fourcc;
    private String audiostream_codecpar_sample_rate;

    public String getCreation_time() {
        return creation_time;
    }

    public void setCreation_time(String creation_time) {
        this.creation_time = creation_time;
    }

    public String getIformat_name() {
        return iformat_name;
    }

    public void setIformat_name(String iformat_name) {
        this.iformat_name = iformat_name;
    }

    public String getVideostream_nb_frames() {
        return videostream_nb_frames;
    }

    public void setVideostream_nb_frames(String videostream_nb_frames) {
        this.videostream_nb_frames = videostream_nb_frames;
    }

    public String getProtocol_blacklist() {
        return protocol_blacklist;
    }

    public void setProtocol_blacklist(String protocol_blacklist) {
        this.protocol_blacklist = protocol_blacklist;
    }

    public String getVideostream_codecpar_bit_rate() {
        return videostream_codecpar_bit_rate;
    }

    public void setVideostream_codecpar_bit_rate(String videostream_codecpar_bit_rate) {
        this.videostream_codecpar_bit_rate = videostream_codecpar_bit_rate;
    }

    public String getVideostream_codecpar_height() {
        return videostream_codecpar_height;
    }

    public void setVideostream_codecpar_height(String videostream_codecpar_height) {
        this.videostream_codecpar_height = videostream_codecpar_height;
    }

    public String getMinor_version() {
        return minor_version;
    }

    public void setMinor_version(String minor_version) {
        this.minor_version = minor_version;
    }

    public String getVideostream_sample_aspect_ratio_num() {
        return videostream_sample_aspect_ratio_num;
    }

    public void setVideostream_sample_aspect_ratio_num(String videostream_sample_aspect_ratio_num) {
        this.videostream_sample_aspect_ratio_num = videostream_sample_aspect_ratio_num;
    }

    public String getVideostream_codec_time_base() {
        return videostream_codec_time_base;
    }

    public void setVideostream_codec_time_base(String videostream_codec_time_base) {
        this.videostream_codec_time_base = videostream_codec_time_base;
    }

    public String getAudiostream_avcodocname() {
        return audiostream_avcodocname;
    }

    public void setAudiostream_avcodocname(String audiostream_avcodocname) {
        this.audiostream_avcodocname = audiostream_avcodocname;
    }

    public String getVideostream_size() {
        return videostream_size;
    }

    public void setVideostream_size(String videostream_size) {
        this.videostream_size = videostream_size;
    }

    public String getVideostream_codecpar_width() {
        return videostream_codecpar_width;
    }

    public void setVideostream_codecpar_width(String videostream_codecpar_width) {
        this.videostream_codecpar_width = videostream_codecpar_width;
    }

    public String getVideostream_codec_fourcc() {
        return videostream_codec_fourcc;
    }

    public void setVideostream_codec_fourcc(String videostream_codec_fourcc) {
        this.videostream_codec_fourcc = videostream_codec_fourcc;
    }

    public String getVideostream_profilestring() {
        return videostream_profilestring;
    }

    public void setVideostream_profilestring(String videostream_profilestring) {
        this.videostream_profilestring = videostream_profilestring;
    }

    public String getVideostream_avg_frame_rate() {
        return videostream_avg_frame_rate;
    }

    public void setVideostream_avg_frame_rate(String videostream_avg_frame_rate) {
        this.videostream_avg_frame_rate = videostream_avg_frame_rate;
    }

    public String getVideostream_avcodocname() {
        return videostream_avcodocname;
    }

    public void setVideostream_avcodocname(String videostream_avcodocname) {
        this.videostream_avcodocname = videostream_avcodocname;
    }

    public String getVideostream_sample_aspect_ratio_den() {
        return videostream_sample_aspect_ratio_den;
    }

    public void setVideostream_sample_aspect_ratio_den(String videostream_sample_aspect_ratio_den) {
        this.videostream_sample_aspect_ratio_den = videostream_sample_aspect_ratio_den;
    }

    public String getAudiostream_size() {
        return audiostream_size;
    }

    public void setAudiostream_size(String audiostream_size) {
        this.audiostream_size = audiostream_size;
    }

    public String getIformat_long_name() {
        return iformat_long_name;
    }

    public void setIformat_long_name(String iformat_long_name) {
        this.iformat_long_name = iformat_long_name;
    }

    public String getProtocol_whitelist() {
        return protocol_whitelist;
    }

    public void setProtocol_whitelist(String protocol_whitelist) {
        this.protocol_whitelist = protocol_whitelist;
    }

    public String getVideostream_duration() {
        return videostream_duration;
    }

    public void setVideostream_duration(String videostream_duration) {
        this.videostream_duration = videostream_duration;
    }

    public String getFilesize() {
        return filesize;
    }

    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBit_rate() {
        return bit_rate;
    }

    public void setBit_rate(String bit_rate) {
        this.bit_rate = bit_rate;
    }

    public String getMax_picture_buffer() {
        return max_picture_buffer;
    }

    public void setMax_picture_buffer(String max_picture_buffer) {
        this.max_picture_buffer = max_picture_buffer;
    }

    public String getVideostream_r_frame_rate() {
        return videostream_r_frame_rate;
    }

    public void setVideostream_r_frame_rate(String videostream_r_frame_rate) {
        this.videostream_r_frame_rate = videostream_r_frame_rate;
    }

    public String getMax_interleave_delta() {
        return max_interleave_delta;
    }

    public void setMax_interleave_delta(String max_interleave_delta) {
        this.max_interleave_delta = max_interleave_delta;
    }

    public String getAudiostream_codecpar_channels() {
        return audiostream_codecpar_channels;
    }

    public void setAudiostream_codecpar_channels(String audiostream_codecpar_channels) {
        this.audiostream_codecpar_channels = audiostream_codecpar_channels;
    }

    public String getAudiostream_profilestring() {
        return audiostream_profilestring;
    }

    public void setAudiostream_profilestring(String audiostream_profilestring) {
        this.audiostream_profilestring = audiostream_profilestring;
    }

    public String getVideostream_time_base() {
        return videostream_time_base;
    }

    public void setVideostream_time_base(String videostream_time_base) {
        this.videostream_time_base = videostream_time_base;
    }

    public String getCompatible_brands() {
        return compatible_brands;
    }

    public void setCompatible_brands(String compatible_brands) {
        this.compatible_brands = compatible_brands;
    }

    public String getMax_ts_probe() {
        return max_ts_probe;
    }

    public void setMax_ts_probe(String max_ts_probe) {
        this.max_ts_probe = max_ts_probe;
    }

    public String getVideostream_codecpar_bits_per_coded_sample() {
        return videostream_codecpar_bits_per_coded_sample;
    }

    public void setVideostream_codecpar_bits_per_coded_sample(String videostream_codecpar_bits_per_coded_sample) {
        this.videostream_codecpar_bits_per_coded_sample = videostream_codecpar_bits_per_coded_sample;
    }

    public String getVideostream_time_base_num() {
        return videostream_time_base_num;
    }

    public void setVideostream_time_base_num(String videostream_time_base_num) {
        this.videostream_time_base_num = videostream_time_base_num;
    }

    public String getMajor_brand() {
        return major_brand;
    }

    public void setMajor_brand(String major_brand) {
        this.major_brand = major_brand;
    }

    public String getPix_fmt_name() {
        return pix_fmt_name;
    }

    public void setPix_fmt_name(String pix_fmt_name) {
        this.pix_fmt_name = pix_fmt_name;
    }

    public String getVideostream_codecpar_codec_type() {
        return videostream_codecpar_codec_type;
    }

    public void setVideostream_codecpar_codec_type(String videostream_codecpar_codec_type) {
        this.videostream_codecpar_codec_type = videostream_codecpar_codec_type;
    }

    public String getVideostream_time_base_den() {
        return videostream_time_base_den;
    }

    public void setVideostream_time_base_den(String videostream_time_base_den) {
        this.videostream_time_base_den = videostream_time_base_den;
    }

    public String getAudiostream_codecpar_codec_type() {
        return audiostream_codecpar_codec_type;
    }

    public void setAudiostream_codecpar_codec_type(String audiostream_codecpar_codec_type) {
        this.audiostream_codecpar_codec_type = audiostream_codecpar_codec_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getVideostream_codecpar_bits_per_raw_sample() {
        return videostream_codecpar_bits_per_raw_sample;
    }

    public void setVideostream_codecpar_bits_per_raw_sample(String videostream_codecpar_bits_per_raw_sample) {
        this.videostream_codecpar_bits_per_raw_sample = videostream_codecpar_bits_per_raw_sample;
    }

    public String getDisplay_aspect_ratio_den() {
        return display_aspect_ratio_den;
    }

    public void setDisplay_aspect_ratio_den(String display_aspect_ratio_den) {
        this.display_aspect_ratio_den = display_aspect_ratio_den;
    }

    public String getAudiostream_codecpar_bit_rate() {
        return audiostream_codecpar_bit_rate;
    }

    public void setAudiostream_codecpar_bit_rate(String audiostream_codecpar_bit_rate) {
        this.audiostream_codecpar_bit_rate = audiostream_codecpar_bit_rate;
    }

    public String getAudiostream_duration() {
        return audiostream_duration;
    }

    public void setAudiostream_duration(String audiostream_duration) {
        this.audiostream_duration = audiostream_duration;
    }

    public String getDisplay_aspect_ratio_num() {
        return display_aspect_ratio_num;
    }

    public void setDisplay_aspect_ratio_num(String display_aspect_ratio_num) {
        this.display_aspect_ratio_num = display_aspect_ratio_num;
    }

    public String getAudiostream_codec_fourcc() {
        return audiostream_codec_fourcc;
    }

    public void setAudiostream_codec_fourcc(String audiostream_codec_fourcc) {
        this.audiostream_codec_fourcc = audiostream_codec_fourcc;
    }

    public String getAudiostream_codecpar_sample_rate() {
        return audiostream_codecpar_sample_rate;
    }

    public void setAudiostream_codecpar_sample_rate(String audiostream_codecpar_sample_rate) {
        this.audiostream_codecpar_sample_rate = audiostream_codecpar_sample_rate;
    }
}
