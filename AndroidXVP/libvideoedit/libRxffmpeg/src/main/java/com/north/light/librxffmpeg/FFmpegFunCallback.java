package com.north.light.librxffmpeg;

/**
 * ffmpeg 监听
 * */
public interface FFmpegFunCallback {
    void failed(String message);
    void success(String path);
    void cancel();
}
