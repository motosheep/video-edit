package com.north.light.librxffmpeg;

/**
 * ffmpeg 监听
 * */
public interface FFmpegFunNewCallback {
    void failed(String message);
    void success(String path);
    void progress(int progress);
    void cancel();
}
