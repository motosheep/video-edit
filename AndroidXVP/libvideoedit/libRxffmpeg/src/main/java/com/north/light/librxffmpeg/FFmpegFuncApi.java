package com.north.light.librxffmpeg;

import android.content.Context;

import com.north.light.librxffmpeg.bean.FFmpegVideoInfo;

import java.io.Serializable;

/**
 * author:li
 * date:2022/3/22
 * desc:ffmpeg func api
 */
public interface FFmpegFuncApi extends Serializable {

    /**
     * 初始化
     */
    void init(Context context);

    /**
     * 获取视频信息
     *
     * @param path 原资源路径
     */
    FFmpegVideoInfo getMediaInfo(String path);

    /**
     * 释放
     */
    void release();

    /**
     * 取消
     */
    void cancel();

    /**
     * 剪裁视频
     */
    void crop(String path, String startTime, String endTime, String outPath, FFmpegFunCallback listener);

    /**
     * 压缩视频
     */
    void compress(String path, String outPath, FFmpegFunNewCallback listener);

    /**
     * 视频添加文字
     */
    void addTxt(String videoPath, String picPath, String outPath, FFmpegFunCallback listener);
}
