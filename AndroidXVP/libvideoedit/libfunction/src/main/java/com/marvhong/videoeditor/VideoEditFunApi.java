package com.marvhong.videoeditor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.marvhong.videoeditor.listener.VideoEditFunCallback;
import com.marvhong.videoeditor.listener.VideoEditFunPicCallback;

public interface VideoEditFunApi {

    /**
     * 初始化
     * */
    void init(Context context);

    /**
     * 启动视频剪辑--读写权限
     */
    void cropVideo(Activity context, String path);

    /**
     * 视频添加单屏文字
    * */
    void addText(Activity activity,String path);

    /**
     * 监听事件
     */
    void setOnFunCallback(VideoEditFunCallback callback);

    void removeFunCallback(VideoEditFunCallback callback);

    void setOnPicCallback(VideoEditFunPicCallback callback);

    void removePicCallback(VideoEditFunPicCallback callback);
}
