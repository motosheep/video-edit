package com.marvhong.videoeditor.listener;

import android.widget.ImageView;

/**
 * 读取图片返回
 */
public interface VideoEditFunPicCallback {
    void loadImage(String path, ImageView view);

    void loadImage(int resId, ImageView view);
}
