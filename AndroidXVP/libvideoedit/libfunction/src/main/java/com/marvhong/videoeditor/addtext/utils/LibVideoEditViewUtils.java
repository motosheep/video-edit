package com.marvhong.videoeditor.addtext.utils;

import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

/**
 * Created by lzt
 * time 2020/10/27
 * 描述：控件工具类
 */
public class LibVideoEditViewUtils implements Serializable {

    private static final String TAG = LibVideoEditViewUtils.class.getSimpleName();

    /**
     * 对View进行截图
     */
    public interface ViewSnapListener {
        void success(Bitmap bitmap);

        void failed();
    }

    public static void viewSnapshot(View view, ViewSnapListener listener) {
        //使控件可以进行缓存
        view.setDrawingCacheEnabled(true);
        //获取缓存的 Bitmap
        Bitmap drawingCache = view.getDrawingCache();
        //复制获取的 Bitmap
        drawingCache = Bitmap.createBitmap(drawingCache);
        //关闭视图的缓存
        view.setDrawingCacheEnabled(false);
        if (drawingCache != null) {
            if (listener != null) {
                listener.success(drawingCache);
            }
        } else {
            if (listener != null) {
                listener.failed();
            }
        }
    }


    public static String saveBitmap(Bitmap bm, String path, String name) {
        Log.e(TAG, "保存图片");
        File fileDir = new File(path);
        if (!fileDir.exists()) {
            createFile(fileDir, false);
        }
        File f = new File(path + name);
        if (!fileDir.exists()) {
            createFile(f, true);
        }
        try {
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();
            return path + name;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void createFile(File file, boolean isFile) {// 创建文件
        if (!file.exists()) {// 如果文件不存在
            if (!file.getParentFile().exists()) {// 如果文件父目录不存在
                createFile(file.getParentFile(), false);
            } else {// 存在文件父目录
                if (isFile) {// 创建文件
                    try {
                        file.createNewFile();// 创建新文件
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    file.mkdir();// 创建目录
                }
            }
        }
    }
}
