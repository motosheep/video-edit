package com.marvhong.videoeditor.crop.utils;

/**
 * 时间工具类
 * */
public class TimeUtils {
    /**
     * 返回日时分秒
     *
     * @return
     */
    public static String calHourMinSecond(long second) {
        long h = 0;
        long d = 0;
        long s = 0;
        long temp = second % 3600;
        if (second > 3600) {
            h = second / 3600;
            if (temp != 0) {
                if (temp > 60) {
                    d = temp / 60;
                    if (temp % 60 != 0) {
                        s = temp % 60;
                    }
                } else {
                    s = temp;
                }
            }
        } else {
            d = second / 60;
            if (second % 60 != 0) {
                s = second % 60;
            }
        }
        return String.format("%02d", h) + ":" + String.format("%02d", d) + ":" + String.format("%02d", s);
    }
}
