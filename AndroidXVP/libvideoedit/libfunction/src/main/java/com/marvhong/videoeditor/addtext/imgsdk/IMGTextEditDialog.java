package com.marvhong.videoeditor.addtext.imgsdk;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.blankj.utilcode.util.KeyboardUtils;
import com.marvhong.videoeditor.R;
import com.marvhong.videoeditor.addtext.imgsdk.core.IMGText;
import com.marvhong.videoeditor.addtext.imgsdk.view.IMGColorGroup;

/**
 * Created by felix on 2017/12/1 上午11:21.
 */

public class IMGTextEditDialog extends Dialog implements View.OnClickListener,
        RadioGroup.OnCheckedChangeListener, CompoundButton.OnCheckedChangeListener {

    private static final String TAG = "IMGTextEditDialog";

    private EditText mEditText;

    private Callback mCallback;
    private CancelCallBack mCancelCallBack;

    private IMGText mDefaultText;

    private IMGColorGroup mColorGroup;
    private boolean isShow = false;
    private CheckBox mCheckBox;

    public IMGTextEditDialog(Context context, Callback callback, CancelCallBack cancelCallBack) {
        super(context, R.style.ImageTextDialog);
        setContentView(R.layout.lib_video_eidt_image_text_dialog);
        mCallback = callback;
        mCancelCallBack = cancelCallBack;
        Window window = getWindow();
        if (window != null) {
            window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        }
    }

    public void release() {
        dismiss();
        mCallback = null;
        mCancelCallBack = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mColorGroup = findViewById(R.id.cg_colors);
        mColorGroup.setOnCheckedChangeListener(this);
        mEditText = findViewById(R.id.et_text);
        mCheckBox = findViewById(R.id.checkBox);
        mCheckBox.setOnCheckedChangeListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
        findViewById(R.id.tv_done).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mDefaultText != null) {
            mEditText.setText(mDefaultText.getText());
            mEditText.setTextColor(mDefaultText.getColor());
            if (!mDefaultText.isEmpty()) {
                mEditText.setSelection(mEditText.length());
            }
            mDefaultText = null;
        } else {
            mEditText.setText("");
        }
        mColorGroup.setCheckColor(mEditText.getCurrentTextColor());
        KeyboardUtils.showSoftInput(mEditText);
    }

    public void setText(IMGText text) {
        mDefaultText = text;
    }

    public void reset() {
        setText(new IMGText(null, Color.WHITE));
    }

    @Override
    public void onClick(View v) {
        int vid = v.getId();
        if (vid == R.id.tv_done) {
            onDone();
        } else if (vid == R.id.tv_cancel) {
            dismiss();
            if (mCancelCallBack != null) {
                mCancelCallBack.autoCancel();
            }
        }
    }

    private void onDone() {
        String text = mEditText.getText().toString();
        if (!TextUtils.isEmpty(text) && mCallback != null) {
            mCallback.onText(new IMGText(text, mEditText.getCurrentTextColor()));
        }
        dismiss();
    }


    @Override
    public void dismiss() {
        KeyboardUtils.hideSoftInput(mEditText);
        super.dismiss();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (mCheckBox.isChecked()) {
            mEditText.setBackgroundColor(mColorGroup.getCheckColor());
            mEditText.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            mEditText.setBackgroundColor(Color.parseColor("#00000000"));
            mEditText.setTextColor(mColorGroup.getCheckColor());
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            mEditText.setBackgroundColor(mColorGroup.getCheckColor());
            mEditText.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            mEditText.setBackgroundColor(Color.parseColor("#00000000"));
            mEditText.setTextColor(mColorGroup.getCheckColor());
        }
    }

    public interface Callback {
        void onText(IMGText text);
    }

    public interface CancelCallBack {
        void autoCancel();
    }

}
