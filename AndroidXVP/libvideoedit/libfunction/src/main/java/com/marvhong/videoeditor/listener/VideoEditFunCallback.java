package com.marvhong.videoeditor.listener;

/**
 * ffmpeg回调
 * */
public interface VideoEditFunCallback {

    /**
     * 剪辑完成的结果
     * */
    void cropPath(String path);

    /**
     * 添加文字的结果
     * */
    void addTxt(String path);

    /**
     * 错误
     * */
    void error(String message);

}
