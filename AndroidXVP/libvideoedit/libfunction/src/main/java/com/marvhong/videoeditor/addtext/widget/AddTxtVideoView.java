package com.marvhong.videoeditor.addtext.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.VideoView;

public class AddTxtVideoView extends VideoView {
    public AddTxtVideoView(Context context) {
        super(context);
    }

    public AddTxtVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AddTxtVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
