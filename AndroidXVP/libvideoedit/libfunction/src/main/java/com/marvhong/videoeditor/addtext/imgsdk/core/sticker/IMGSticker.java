package com.marvhong.videoeditor.addtext.imgsdk.core.sticker;


import com.marvhong.videoeditor.addtext.imgsdk.core.IMGViewPortrait;

/**
 * Created by felix on 2017/11/14 下午7:31.
 */

public interface IMGSticker extends IMGStickerPortrait, IMGViewPortrait {

}
