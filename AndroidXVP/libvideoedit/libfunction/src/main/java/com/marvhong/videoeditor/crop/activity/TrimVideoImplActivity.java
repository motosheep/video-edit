package com.marvhong.videoeditor.crop.activity;

import android.app.Activity;
import android.content.Intent;

/**
 * 视频编辑继承类--外部调用入口
 */
public class TrimVideoImplActivity extends TrimVideoActivity {

    public static void launch(Activity context, String videoPath) {
        Intent intent = new Intent(context, TrimVideoImplActivity.class);
        intent.putExtra(PARAMS_CROP_ORG_PATH, videoPath);
        context.startActivityForResult(intent, TRIM_VIDEO_REQ);
    }

    @Override
    protected void cropFinish(String path) {
        super.cropFinish(path);
    }

    @Override
    protected void editFailed(String message) {
        super.editFailed(message);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
