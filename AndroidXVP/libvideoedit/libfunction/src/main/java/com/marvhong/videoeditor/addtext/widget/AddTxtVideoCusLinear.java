package com.marvhong.videoeditor.addtext.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

public class AddTxtVideoCusLinear extends LinearLayout {
    public AddTxtVideoCusLinear(Context context) {
        super(context);
    }

    public AddTxtVideoCusLinear(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AddTxtVideoCusLinear(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }
}
