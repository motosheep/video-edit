package com.marvhong.videoeditor.addtext.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.marvhong.videoeditor.addtext.imgsdk.view.IMGStickerTextView;
import com.marvhong.videoeditor.addtext.imgsdk.view.IMGView;

public class AddTxtIMGView extends IMGView {
    public AddTxtIMGView(Context context) {
        super(context);
        init();
    }

    public AddTxtIMGView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AddTxtIMGView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                Log.e("AddTxtIMGView", "ACTION_DOWN");
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                Log.e("AddTxtIMGView", "ACTION_UP/CANCEL");
                showHideContent(false);
                break;
            case MotionEvent.ACTION_MOVE:
                break;
        }
        return true;
    }

    /**
     * 初始化
     */
    private void init() {

    }

    /**
     * 显示or隐藏非字体布局
     */
    private void showHideContent(boolean flag) {
        if (getStickerTextView() == null) {
            return;
        }
        if (flag) {
            //显示
            getStickerTextView().getAdjustView().setVisibility(View.VISIBLE);
            getStickerTextView().getRemoveView().setVisibility(View.VISIBLE);
            getStickerTextView().showOrHideLineBar(true);
        } else {
            //隐藏
            getStickerTextView().getAdjustView().setVisibility(View.GONE);
            getStickerTextView().getRemoveView().setVisibility(View.GONE);
            getStickerTextView().showOrHideLineBar(false);
        }
    }

    @Override
    public IMGStickerTextView getStickerTextView() {
        return super.getStickerTextView();
    }

    @Override
    public void finishInitStickerTV() {
        super.finishInitStickerTV();
        if (getStickerTextView() == null) {
            return;
        }
        getStickerTextView().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //点击了内容控件
                Log.e("AddTxtIMGView", "getStickerTextView click");
                showHideContent(true);
            }
        });
    }
}
