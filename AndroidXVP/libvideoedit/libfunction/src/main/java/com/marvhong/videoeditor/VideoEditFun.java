package com.marvhong.videoeditor;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.widget.ImageView;

import androidx.core.app.ActivityCompat;

import com.marvhong.videoeditor.addtext.ui.VideoAddTextActivity;
import com.marvhong.videoeditor.crop.activity.TrimVideoImplActivity;
import com.marvhong.videoeditor.listener.VideoEditFunCallback;
import com.marvhong.videoeditor.listener.VideoEditFunPicCallback;
import com.north.light.librxffmpeg.FFmpegFunc;

import java.io.File;
import java.util.concurrent.CopyOnWriteArrayList;

public class VideoEditFun implements VideoEditFunApi {
    private CopyOnWriteArrayList<VideoEditFunCallback> callbacksList = new CopyOnWriteArrayList<>();
    private CopyOnWriteArrayList<VideoEditFunPicCallback> picCallbackList = new CopyOnWriteArrayList<>();

    private Context context;

    private static final class SingleHolder {
        private static final VideoEditFun mInstance = new VideoEditFun();
    }

    public static VideoEditFun getInstance() {
        return VideoEditFun.SingleHolder.mInstance;
    }


    @Override
    public void init(Context context) {
        this.context = context.getApplicationContext();
        FFmpegFunc.getInstance().init(this.context);
    }

    public Context getContext() {
        return context;
    }

    /**
     * 检查传入路径和权限
     * change by lzt 20220323 增加系统版本判断--如果低于19则不可用
     */
    private boolean checkPathAndPermission(String path) {
        try {
            if(Build.VERSION.SDK_INT<Build.VERSION_CODES.LOLLIPOP){
                notifyError("system not support");
                return false;
            }
            if (TextUtils.isEmpty(path)) {
                return false;
            }
            //判断文件是否存在
            File file = new File(path);
            if (!file.exists()) {
                notifyError("file no exist");
                return false;
            }
            //判断是否有读写权限
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                notifyError("please grant write/read permission");
                return false;
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 检查文件存在性，检查权限可读性
     */
    @Override
    public void cropVideo(Activity context, String path) {
        if (!checkPathAndPermission(path)) {
            return;
        }
        TrimVideoImplActivity.launch(context, path);
    }

    /**
     * 视频添加单屏文字
     */
    @Override
    public void addText(Activity activity, String path) {
        if (!checkPathAndPermission(path)) {
            return;
        }
        VideoAddTextActivity.launch(activity, path);
    }


    //设置监听----------------------------------------------------------------------------
    @Override
    public void setOnFunCallback(VideoEditFunCallback callback) {
        callbacksList.add(callback);
    }

    @Override
    public void removeFunCallback(VideoEditFunCallback callback) {
        callbacksList.remove(callback);
    }

    @Override
    public void setOnPicCallback(VideoEditFunPicCallback callback) {
        picCallbackList.add(callback);
    }

    @Override
    public void removePicCallback(VideoEditFunPicCallback callback) {
        picCallbackList.remove(callback);
    }

    //回调事件--内部调用，通知外部---------------------------------------------------------------
    public void notifyPic(String path, ImageView img) {
        for (VideoEditFunPicCallback callback : picCallbackList) {
            if (callback != null && img != null) {
                img.setScaleType(ImageView.ScaleType.CENTER_CROP);
                callback.loadImage(path, img);
            }
        }
    }

    public void notifyError(String message) {
        for (VideoEditFunCallback callback : callbacksList) {
            if (callback != null) {
                callback.error(message);
            }
        }
    }

    public void notifyCropResult(String path) {
        for (VideoEditFunCallback callback : callbacksList) {
            if (callback != null) {
                callback.cropPath(path);
            }
        }
    }

    public void notifyAddTextResult(String path) {
        for (VideoEditFunCallback callback : callbacksList) {
            if (callback != null) {
                callback.addTxt(path);
            }
        }
    }


}
