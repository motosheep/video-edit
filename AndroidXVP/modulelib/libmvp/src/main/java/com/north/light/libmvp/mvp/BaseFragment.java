package com.north.light.libmvp.mvp;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.north.light.libcommon.toast.ToastUtils;
import com.north.light.libmvp.mvp.base.BaseMvpLazyFragment;
import com.north.light.libmvp.mvp.base.IMvpBasePresenter;

import java.util.List;

/**
 * change by lzt 20200516 增加init event抽象方法（规避kotlin空指针问题）
 * 一般外部模块，继承该基础activity作为父类
 * */
public abstract class BaseFragment<T extends IMvpBasePresenter> extends BaseMvpLazyFragment<T> {

    /**
     * 运行于onCreateView方法后
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void initData() {

    }

    @Override
    public void shortToast(String text) {
        if (TextUtils.isEmpty(text)) return;
        ToastUtils.toast(getActivity().getApplicationContext(), text, Toast.LENGTH_SHORT);
    }

    @Override
    public void longToast(String text) {
        if (TextUtils.isEmpty(text)) return;
        ToastUtils.toast(getActivity().getApplicationContext(), text, Toast.LENGTH_LONG);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    /**
     * 权限申请
     * 注意读写权限适配：Permission.MANAGE_EXTERNAL_STORAGE
     */
    public void requestPermission(final Boolean enterSetting, String... permissions) {

    }

    /**
     * 权限授权结果
     *
     * @param status 1授权成功 2授权失败（非永久） 3授权失败(永久)
     */
    public void permissionGrantResult(int status) {

    }

}
