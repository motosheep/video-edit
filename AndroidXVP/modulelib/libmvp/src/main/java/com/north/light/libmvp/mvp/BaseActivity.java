package com.north.light.libmvp.mvp;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.north.light.libcommon.toast.ToastUtils;
import com.north.light.libmvp.mvp.base.BaseMvpActivity;
import com.north.light.libmvp.mvp.base.IMvpBasePresenter;
import com.north.light.libmvp.stack.ActivityStack;

import java.util.List;

abstract class BaseActivity<T extends IMvpBasePresenter> extends BaseMvpActivity<T> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActivityStack.getInstance().put(getClass().getSimpleName(), this);
        super.onCreate(savedInstanceState);
        init();
    }

    @Override
    protected void onDestroy() {
        ActivityStack.getInstance().out(getClass().getSimpleName());
        super.onDestroy();
    }

    private void init() {

    }

    @Override
    public void shortToast(String text) {
        if (TextUtils.isEmpty(text)) return;
        ToastUtils.toast(this.getApplicationContext(), text, Toast.LENGTH_SHORT);
    }

    @Override
    public void longToast(String text) {
        if (TextUtils.isEmpty(text)) return;
        ToastUtils.toast(this.getApplicationContext(), text, Toast.LENGTH_LONG);
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {
    }

    /**
     * 权限申请
     * 注意读写权限适配：Permission.MANAGE_EXTERNAL_STORAGE
     */
    public void requestPermission(final Boolean enterSetting, String... permissions) {

    }

    /**
     * 权限授权结果
     *
     * @param status 1授权成功 2授权失败（非永久） 3授权失败(永久)
     */
    public void permissionGrantResult(int status) {

    }
}
