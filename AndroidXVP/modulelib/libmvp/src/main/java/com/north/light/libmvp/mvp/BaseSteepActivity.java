package com.north.light.libmvp.mvp;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.north.light.libmvp.R;
import com.north.light.libmvp.mvp.base.IMvpBasePresenter;
import com.north.light.libmvp.mvp.base.ScreenUtils;

/**
 * 沉浸式基类--一般外部模块，继承该基础activity作为父类
 */
public abstract class BaseSteepActivity<T extends IMvpBasePresenter> extends BaseActivity<T> {

    public abstract int statusColor();

    public abstract int backgroundRes();

    public abstract boolean isNeedStatus();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            // 透明状态栏
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
//                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
//            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        //区分4.4和5.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //5.0
            View RootView = LayoutInflater.from(this).inflate(R.layout.activity_base, null);
            View ActivityView = LayoutInflater.from(this).inflate(getLayout(), null);
            LinearLayout linearLayout = RootView.findViewById(R.id.activity_base_content);
            linearLayout.addView(ActivityView);
            //设置状态栏高度
            if (isNeedStatus()) {
                LinearLayout statusLinear = RootView.findViewById(R.id.activity_base_status);
                LinearLayout.LayoutParams statusParams = (LinearLayout.LayoutParams) statusLinear.getLayoutParams();
                statusParams.height = ScreenUtils.getStatusBarHeight(this);
                statusLinear.setLayoutParams(statusParams);
                //设置状态栏颜色
                if (statusColor() != 0) {
                    statusLinear.setBackgroundResource(statusColor());
                }
            }
            //设置content宽高
            LinearLayout.LayoutParams contentParams = (LinearLayout.LayoutParams) ActivityView.getLayoutParams();
            contentParams.height = LinearLayout.LayoutParams.MATCH_PARENT;
            contentParams.weight = 1;
            ActivityView.setLayoutParams(contentParams);
            //设置根布局背景
            if (backgroundRes() != 0) {
                RootView.findViewById(R.id.activity_base_parent).setBackgroundResource(backgroundRes());
            }
            setContentView(RootView);
        } else {
            //4.4
            setContentView(getLayout());
        }
    }

    public abstract int getLayout();

}
