package com.north.light.libmvp.router;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.IdRes;

import com.north.light.libmvp.R;

import java.util.ArrayList;

/**
 * create by lzt
 * data 2019/12/10
 */
public class XRouter {
    private RouterInfo mInfo;

    private static class SingleHolder {
        static final XRouter mInstance = new XRouter();
    }

    public static XRouter getInstance() {
        return SingleHolder.mInstance;
    }

    //首次调用__必须调用
    public XRouter newRouter() {
        mInfo = new RouterInfo();
        return this;
    }

    public XRouter putInt(String intTag, int intP) {
        if (mInfo != null || !TextUtils.isEmpty(intTag)) {
            mInfo.getBundle().putInt(intTag, intP);
        }
        return this;
    }

    public XRouter putString(String stringTag, String stringP) {
        if (mInfo != null || !TextUtils.isEmpty(stringTag)) {
            mInfo.getBundle().putString(stringTag, stringP);
        }
        return this;
    }

    public XRouter putStringList(String listTag, ArrayList<String> mParamsList) {
        if (mInfo != null || !TextUtils.isEmpty(listTag)) {
            mInfo.getBundle().putStringArrayList(listTag, mParamsList);
        }
        return this;
    }

    public XRouter putLong(String longTag, long longP) {
        if (mInfo != null || !TextUtils.isEmpty(longTag)) {
            mInfo.getBundle().putLong(longTag, longP);
        }
        return this;
    }

    public XRouter putRequestCode(String requestCodeTAG, int requestCode) {
        if (mInfo != null || !TextUtils.isEmpty(requestCodeTAG)) {
            mInfo.mTAGRequest = requestCodeTAG;
            mInfo.mParamsRequestCode = requestCode;
        }
        return this;
    }

    public XRouter setAnim(@IdRes int enter, @IdRes int out) {
        if (mInfo != null) {
            mInfo.mEnterAnim = enter;
            mInfo.mOutAnim = out;
        }
        return this;
    }


    //最后调用__必须调用
    public void launch(Activity from, Class<?> target) {
        launch(from, target, null);
    }

    //带有回调
    public void launch(Activity from, Class<?> target, RouterCallBack callBack) {
        try {
            if (callBack != null) {
                callBack.Before();
            }
            if (mInfo != null && from != null && target != null) {
                Intent mIntent = new Intent();
                mIntent.setClass(from, target);
                mIntent.putExtras(mInfo.getBundle());
                if (!TextUtils.isEmpty(mInfo.mTAGRequest)) {
                    from.startActivityForResult(mIntent, mInfo.mParamsRequestCode);
                } else {
                    from.startActivity(mIntent);
                }
                if (mInfo.mEnterAnim != 0 && mInfo.mOutAnim != 0) {
                    from.overridePendingTransition(mInfo.mEnterAnim, mInfo.mOutAnim);
                }
            }
            if (callBack != null) {
                callBack.After();
            }
        } catch (Exception e) {
            if (callBack != null) {
                callBack.Error(e);
            }
        }
    }


    public interface RouterCallBack {
        void Before();

        void After();

        void Error(Exception e);
    }

    private class RouterInfo {
        private Bundle mBundle;
        private int mParamsRequestCode;
        private String mTAGRequest;

        private int mEnterAnim = R.anim.anim_top_in;
        private int mOutAnim = R.anim.anim_top_out;


        public Bundle getBundle() {
            if (mBundle == null) mBundle = new Bundle();
            return mBundle;
        }
    }

    public static void backHome(Activity activity) {
        Intent home = new Intent(Intent.ACTION_MAIN);
        home.addCategory(Intent.CATEGORY_HOME);
        activity.startActivity(home);
    }
}


