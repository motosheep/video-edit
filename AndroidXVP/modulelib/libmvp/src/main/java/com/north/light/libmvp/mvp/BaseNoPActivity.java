package com.north.light.libmvp.mvp;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.north.light.libmvp.stack.ActivityStack;
import com.trello.rxlifecycle4.components.support.RxFragmentActivity;

/**
 * Created by lzt
 * time 2021/1/22
 * 描述：没有Presenter的activity
 */
public abstract class BaseNoPActivity extends RxFragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityStack.getInstance().put(getClass().getSimpleName(), this);
        if (getLayout() > 0) {
            setContentView(getLayout());
        }
        init();
    }

    @Override
    protected void onDestroy() {
        ActivityStack.getInstance().out(getClass().getSimpleName());
        super.onDestroy();
    }

    public abstract int getLayout();

    private void init() {
    }
}
