package com.north.light.libmvp.mvp.base;

public interface IMvpBaseView {

    void shortToast(String text);

    void longToast(String text);

    void showLoading();

    void dismissLoading();

}
