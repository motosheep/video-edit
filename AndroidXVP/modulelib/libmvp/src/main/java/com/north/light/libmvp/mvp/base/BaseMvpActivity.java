package com.north.light.libmvp.mvp.base;

import android.os.Bundle;
import android.view.View;

import com.trello.rxlifecycle4.components.support.RxFragmentActivity;


public abstract class BaseMvpActivity<P extends IMvpBasePresenter>  extends RxFragmentActivity implements IMvpBaseView {
    private P presenter;
    public abstract P createPresenter();

    public P getP() {
        return presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ScreenUtils.setCustomDensity(this, this.getApplication());
        presenter = createPresenter();
        if (presenter == null) {
            throw new NullPointerException("Presenter is null! Do you return null in createModel()?");
        }
        presenter.onMvpAttachView(this, savedInstanceState);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (presenter != null) {
            presenter.onMvpStart();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onMvpResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (presenter != null) {
            presenter.onMvpPause();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.onMvpStop();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (presenter != null) {
            presenter.onMvpSaveInstanceState(outState);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onMvpDetachView(false);
            presenter.onMvpDestroy();
        }
    }

    /**
     * findid 函数
     */
    protected <T extends View> T F(int resId) {
        return (T) super.findViewById(resId);
    }

}
