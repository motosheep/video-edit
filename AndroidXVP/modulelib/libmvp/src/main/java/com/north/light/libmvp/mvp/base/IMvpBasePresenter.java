package com.north.light.libmvp.mvp.base;

import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;

public interface IMvpBasePresenter<V extends IMvpBaseView> {

    void onMvpAttachView(V view, Bundle savedInstanceState);

    void onMvpStart();

    void onMvpResume();

    void onMvpPause();

    void onMvpStop();

    void onMvpSaveInstanceState(Bundle savedInstanceState);

    void onMvpDetachView(boolean retainInstance);

    void onMvpDestroy();

}
