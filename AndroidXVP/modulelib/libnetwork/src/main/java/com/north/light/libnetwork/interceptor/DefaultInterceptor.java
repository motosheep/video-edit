package com.north.light.libnetwork.interceptor;

import java.io.IOException;

import okhttp3.Request;

/**
 * Created by lzt
 * time 2021/1/21
 * 描述：默认拦截器
 */
public class DefaultInterceptor extends BaseInterceptor {

    @Override
    public Request getRequest(Chain chain) throws IOException {
        return chain.request();
    }
}
