package com.north.light.libnetwork.base;

import android.app.Activity;
import android.text.TextUtils;

import com.north.light.libnetwork.bean.BaseCode;
import com.north.light.libnetwork.interceptor.DefaultInterceptor;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * create by lzt
 * data 2019/12/21
 * 网络通信基类
 */
public abstract class BaseNetWork extends NetMain {
    private Retrofit mRetrofit;

    public void init(String url) {
        if (TextUtils.isEmpty(url)) return;
        Retrofit retrofit = getRetrofit(url);
        OkHttpClient httpClient = new OkHttpClient.Builder()
                .sslSocketFactory(SSLSocketClient.getSSLSocketFactory())
                .hostnameVerifier(SSLSocketClient.getHostnameVerifier())
                .connectTimeout(timeOut(), TimeUnit.SECONDS)//设置连接超时时间
                .addInterceptor((interceptor() == null) ? new DefaultInterceptor() : this.interceptor())
                .build();
        if (retrofit != null) {
            mRetrofit = retrofit;
        } else {
            mRetrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                    .build();
        }
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public abstract Interceptor interceptor();

    public abstract int timeOut();
}

