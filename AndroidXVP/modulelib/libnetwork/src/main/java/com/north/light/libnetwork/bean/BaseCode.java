package com.north.light.libnetwork.bean;

import java.io.Serializable;

/**
 * Created by lzt
 * time 2021/1/21
 * 描述：网络请求基础code
 */
public class BaseCode implements Serializable {
    public String code;
    public  String msg;
}
