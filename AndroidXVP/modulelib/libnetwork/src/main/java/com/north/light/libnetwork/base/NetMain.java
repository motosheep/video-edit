package com.north.light.libnetwork.base;


import com.north.light.libnetwork.manager.RetrofitManager;

import retrofit2.Retrofit;

/**
 * 外部获取retrofit对象的类
 * 统一管理retrofit对象
 */
public abstract class NetMain {

    public void setRetrofit(String baseUrl, Retrofit retrofit){
        RetrofitManager.getInstance().setRetrofit(baseUrl, retrofit);
    }

    public Retrofit getRetrofit(String baseUrl) {
        return RetrofitManager.getInstance().getRetrofit(baseUrl);
    }


}
