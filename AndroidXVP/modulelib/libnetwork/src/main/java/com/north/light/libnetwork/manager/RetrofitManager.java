package com.north.light.libnetwork.manager;

import android.text.TextUtils;

import java.util.concurrent.ConcurrentHashMap;

import retrofit2.Retrofit;

public class RetrofitManager {

    private ConcurrentHashMap<String, Retrofit> mRetrofitMap = new ConcurrentHashMap<>();

    private static class SingleHolder {
        static final RetrofitManager mInstance = new RetrofitManager();
    }

    public static RetrofitManager getInstance() {
        return SingleHolder.mInstance;
    }

    public Retrofit getRetrofit(String baseUrl) {
        if (TextUtils.isEmpty(baseUrl) || mRetrofitMap.get(baseUrl) == null) return null;
        else return mRetrofitMap.get(baseUrl);
    }

    public void setRetrofit(String baseUrl, Retrofit retrofit) {
        mRetrofitMap.put(baseUrl, retrofit);
    }
}
