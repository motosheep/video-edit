package com.north.light.libnetwork.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 请求的基类
 * 抽象出intercept方法，用于实现具体逻辑
 * */
public abstract class BaseInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        return chain.proceed(getRequest(chain));
    }

    public abstract Request getRequest(Chain chain) throws IOException;
}
