package com.north.light.libosspic;

import android.content.Context;
import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;

/**
 * Created by lzt
 * time 2020/8/15
 * 描述：oss pic manager
 * 若本地的 BUCKET_NAME为空orENDPOINT为空,则通过接口请求，然后拼接参数
 */

public class OSSPicManager implements Serializable {
    private Context mContext;

    private static final class SingleHolder {
        static OSSPicManager mInstance = new OSSPicManager();
    }

    public static OSSPicManager getInstance() {
        return SingleHolder.mInstance;
    }

    public void init(Context context){
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    /**
     * 判断本地数据是否为空，否则通过接口获取key,endpoint
     * */
    public Observable<List<String>> uploadPic(final List<String> picList) {
//        if (TextUtils.isEmpty(OSSConstant.OSS_PIC_ENDPOINT) ||
//                TextUtils.isEmpty(OSSConstant.OSS_PIC_KEY)) {
//            return NetWorkUtils.getInstance().getOSSPicParams("1")
//                    .map(new Function<OSSPicParamsResponse, OSSPicParamsResponse>() {
//                        @Override
//                        public OSSPicParamsResponse apply(OSSPicParamsResponse ossPicParamsResponse) throws Exception {
//                            if (ossPicParamsResponse != null) {
//                                //key
//                                OSSConstant.OSS_PIC_KEY = ossPicParamsResponse.getAccessid();
//                                //剪切host
//                                String host = ossPicParamsResponse.getHost();
//                                if (!TextUtils.isEmpty(host)) {
//                                    int startPos = host.indexOf(".") + 1;
//                                    host = "https://" + host.substring(startPos);
//                                    OSSConstant.OSS_PIC_ENDPOINT = host;
//                                }
//                            }
//                            return ossPicParamsResponse;
//                        }
//                    })
//                    .flatMap(new Function<OSSPicParamsResponse, ObservableSource<List<String>>>() {
//                        @Override
//                        public ObservableSource<List<String>> apply(OSSPicParamsResponse ossPicParamsResponse) throws Exception {
//                            return Observable.create(new ObservableOnSubscribe<List<String>>() {
//                                @Override
//                                public void subscribe(ObservableEmitter<List<String>> e) throws Exception {
//                                    List<String> result = new ArrayList<>();
//                                    for (int i = 0; i < picList.size(); i++) {
//                                        String cache = UploadHelper.uploadImage(picList.get(i));
//                                        if (!TextUtils.isEmpty(cache)) {
//                                            result.add(cache);
//                                        }
//                                    }
//                                    e.onNext(result);
//                                    e.onComplete();
//                                }
//                            }).map(new Function<List<String>, List<String>>() {
//                                @Override
//                                public List<String> apply(List<String> strings) throws Exception {
//                                    return strings;
//                                }
//                            });
//                        }
//                    });
//        } else {
//            return Observable.create(new ObservableOnSubscribe<List<String>>() {
//                @Override
//                public void subscribe(ObservableEmitter<List<String>> e1) throws Exception {
//                    List<String> result = new ArrayList<>();
//                    for (int i = 0; i < picList.size(); i++) {
//                        String cache = UploadHelper.uploadImage(picList.get(i));
//                        if (!TextUtils.isEmpty(cache)) {
//                            result.add(cache);
//                        }
//                    }
//                    e1.onNext(result);
//                    e1.onComplete();
//                }
//            });
//        }
        return Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(ObservableEmitter<List<String>> e1) throws Exception {
                List<String> result = new ArrayList<>();
                for (int i = 0; i < picList.size(); i++) {
                    String cache = UploadHelper.uploadImage(picList.get(i));
                    if (!TextUtils.isEmpty(cache)) {
                        result.add(cache);
                    }
                }
                e1.onNext(result);
                e1.onComplete();
            }
        });

    }
}
