package com.north.light.libloadpic;

import android.content.Context;

import com.north.light.libloadpic.glide.GlideLoader;

/**
 * 使用前必须调用init初始化
 */
class ImageLoader {
    private ImageLoaderApi mLoaderInstance;

    private static final class SingleHolder {
        static final ImageLoader mInstance = new ImageLoader();
    }

    public static ImageLoaderApi getLoader() {
        return SingleHolder.mInstance.mLoaderInstance;
    }

    public ImageLoader() {
        this.mLoaderInstance = new GlideLoader();
    }

    public void init(Context context) {
        mLoaderInstance.init(context.getApplicationContext());
    }


}
