package com.north.light.libloadpic;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.request.target.SimpleTarget;

/**
 * 外部调用-----------------
 * */
public class ImageManager {

    private static class SingleHolder {
        static ImageManager mInstance = new ImageManager();
    }

    public static ImageManager getInstance() {
        return SingleHolder.mInstance;
    }

    /**
     * 初始化函数
     */
    public void init(Context context) {
        //图片加载初始化
        ImageLoader.getLoader().init(context);
    }


    /**
     * 加载圆形图片，由于圆形图片不需要radius大小，width==height，所以只需要传入一个size即可
     */
    public <T extends ImageView> void loadCircleBySize(int size, String url, T t) {
        ImageLoader.getLoader().load(TextUtils.isEmpty(url) ? R.mipmap.ic_load_pic_default_logo : url, t,
                new ImageLoaderApi.ImageSizeOption(size, size),
                new ImageLoaderApi.ImageDefaultOption(R.mipmap.ic_load_pic_place_holder, R.mipmap.ic_load_pic_default_logo),
                new ImageLoaderApi.ImageCropOption(ImageLoaderApi.ImageCropOption.TYPE_CIRCLE, size));
    }

    /**
     * 加载圆角图片
     */
    public <T extends ImageView> void loadRoundBySize(int width, int height, int size, String url, T t) {
        ImageLoader.getLoader().load(TextUtils.isEmpty(url) ? R.mipmap.ic_load_pic_default_logo : url, t,
                new ImageLoaderApi.ImageSizeOption(width, height),
                new ImageLoaderApi.ImageDefaultOption(R.mipmap.ic_load_pic_place_holder, R.mipmap.ic_load_pic_default_logo),
                new ImageLoaderApi.ImageCropOption(ImageLoaderApi.ImageCropOption.TYPE_ROUNE, size));
    }

    /**
     * 加载圆角图片
     */
    public <T extends ImageView> void loadRoundBySizeDefault(int width, int height, int size, String url, T t, int id) {
        ImageLoader.getLoader().load(TextUtils.isEmpty(url) ? id : url, t,
                new ImageLoaderApi.ImageSizeOption(width, height),
                new ImageLoaderApi.ImageDefaultOption(id, id),
                new ImageLoaderApi.ImageCropOption(ImageLoaderApi.ImageCropOption.TYPE_ROUNE, size));
    }

    /**
     * 加载原图
     */
    public <T extends ImageView> void loadNormalPic(String url, T t) {
        ImageLoader.getLoader().load(TextUtils.isEmpty(url) ? R.mipmap.ic_load_pic_place_holder : url, t,
                new ImageLoaderApi.ImageDefaultOption(R.mipmap.ic_load_pic_place_holder, R.mipmap.ic_load_pic_place_holder));
    }

    /**
     * 加载宽高相等的图片
     */
    public <T extends ImageView> void loadNormalPicBySize(int size, String url, T t) {
        ImageLoader.getLoader().load(TextUtils.isEmpty(url) ? R.mipmap.ic_load_pic_place_holder : url, t,
                new ImageLoaderApi.ImageSizeOption(size, size),
                new ImageLoaderApi.ImageDefaultOption(R.mipmap.ic_load_pic_place_holder, R.mipmap.ic_load_pic_place_holder));
    }

    /**
     * 加载指定宽高的图片
     */
    public <T extends ImageView> void loadNormalPicBySize(int width, int height, String url, T t, int defaultId) {
        ImageLoader.getLoader().load(TextUtils.isEmpty(url) ? defaultId : url, t,
                new ImageLoaderApi.ImageSizeOption(width, height),
                new ImageLoaderApi.ImageDefaultOption(defaultId, defaultId));
    }

    /**
     * 加载本地图片
     */
    public <T extends ImageView> void loadLocalPic(int resourceId, T t) {
        ImageLoader.getLoader().load(resourceId, t);
    }


    /**
     * 加载图片并生成target
     */
    public <T> void loadWithTarget(T url, SimpleTarget<Bitmap> target) {
        ImageLoader.getLoader().load(url, target);
    }
}
