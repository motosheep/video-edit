package com.north.light.libloadpic;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;

public interface ImageLoaderApi {
    void init(Context context);

    <T, W extends ImageView> void load(T url, W iv);

    <T, W extends ImageView> void load(T url, W iv, ImageSizeOption option);

    <T, W extends ImageView> void load(T url, W iv, ImageDefaultOption option);

    <T, W extends ImageView> void load(T url, W iv, ImageSizeOption sizeOption, ImageDefaultOption defaultOption);

    <T, W extends ImageView> void load(T url, W iv, ImageSizeOption sizeOption, ImageDefaultOption defaultOption, ImageCropOption cropOption);

    <T> void load(T url, Target<Bitmap> target);

    <T> void load(T url, SimpleTarget<Bitmap> bitmapSimpleTarget);

    void clearMemory();

    void clearDisk() throws Exception;

    void pause(Context context) throws Exception;

    void resume(Context context) throws Exception;

    //图片默认加载图片
    class ImageDefaultOption {
        private int placeId;
        private int errorId;

        public ImageDefaultOption(int placeId, int errorId) {
            this.placeId = placeId;
            this.errorId = errorId;
        }

        public int getPlaceId() {
            return placeId;
        }

        public void setPlaceId(int placeId) {
            this.placeId = placeId;
        }

        public int getErrorId() {
            return errorId;
        }

        public void setErrorId(int errorId) {
            this.errorId = errorId;
        }
    }

    //图片大小配置
    class ImageSizeOption {
        private int resizeWidth = 0;
        private int resizeHeight = 0;

        public ImageSizeOption(int width, int height) {
            this.resizeWidth = width;
            this.resizeHeight = height;
        }

        public int getResizeWidth() {
            return resizeWidth;
        }

        public void setResizeWidth(int resizeWidth) {
            this.resizeWidth = resizeWidth;
        }

        public int getResizeHeight() {
            return resizeHeight;
        }

        public void setResizeHeight(int resizeHeight) {
            this.resizeHeight = resizeHeight;
        }
    }

    //图片剪裁配置
    class ImageCropOption{
        private int type;
        public static final int TYPE_CIRCLE = 1;
        public static final int TYPE_ROUNE = 2;
        private int size;

        public ImageCropOption(int type,int size){
            this.type = type;
            this.size = size;
        }

        public int getType() {
            return type;
        }

        public int getTYPE_CIRCLE() {
            return TYPE_CIRCLE;
        }

        public int getTYPE_ROUNE() {
            return TYPE_ROUNE;
        }

        public int getSize() {
            return size;
        }
    }
}
