package com.north.light.libloadpic.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.north.light.libloadpic.ImageLoaderApi;

public class GlideLoader implements ImageLoaderApi {
    private Context mContext;

    @Override
    public void init(Context context) {
        this.mContext = context.getApplicationContext();
    }

    private RequestManager getGlideManager(Context context) {
        return Glide.with(context);
    }

    @Override
    public <T, W extends ImageView> void load(T url, W iv) {
        getGlideManager(iv.getContext()).load(url).apply(new RequestOptions()).diskCacheStrategy(DiskCacheStrategy.ALL).into(iv);
    }

    @Override
    public <T, W extends ImageView> void load(T url, W iv, ImageSizeOption option) {
        RequestOptions options = new RequestOptions().override(option.getResizeWidth(), option.getResizeHeight());
        getGlideManager(iv.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).apply(options).into(iv);
    }

    @Override
    public <T, W extends ImageView> void load(T url, W iv, ImageDefaultOption option) {
        RequestOptions options = new RequestOptions()
                .placeholder(option.getPlaceId())
                .error(option.getErrorId());
        getGlideManager(iv.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).apply(options).into(iv);
    }

    @Override
    public <T, W extends ImageView> void load(T url, W iv, ImageSizeOption sizeOption, ImageDefaultOption defaultOption) {
        RequestOptions options = new RequestOptions()
                .override(sizeOption.getResizeWidth(), sizeOption.getResizeHeight())
                .placeholder(defaultOption.getPlaceId())
                .error(defaultOption.getErrorId());
        getGlideManager(iv.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).apply(options).into(iv);
    }

    @Override
    public <T, W extends ImageView> void load(T url, W iv, ImageSizeOption sizeOption, ImageDefaultOption defaultOption, ImageCropOption cropOption) {
        if(cropOption.getType()==cropOption.getTYPE_CIRCLE()){
            RequestOptions options = new RequestOptions()
                    .circleCrop()
                    .override(sizeOption.getResizeWidth(), sizeOption.getResizeHeight())
                    .placeholder(defaultOption.getPlaceId())
                    .error(defaultOption.getErrorId());
            getGlideManager(iv.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).apply(options).into(iv);
        }else{
            RequestOptions options = new RequestOptions()
                    .override(sizeOption.getResizeWidth(), sizeOption.getResizeHeight())
                    .placeholder(defaultOption.getPlaceId())
                    .transform(new RoundedCorners(cropOption.getSize()))
                    .error(defaultOption.getErrorId());
            getGlideManager(iv.getContext()).load(url).diskCacheStrategy(DiskCacheStrategy.ALL).apply(options).into(iv);
        }
    }

    @Override
    public <T> void load(T url, Target<Bitmap> target) {
        getGlideManager(mContext).asBitmap().load(url).into(target);
    }

    @Override
    public <T> void load(T url, SimpleTarget<Bitmap> bitmapSimpleTarget) {
        getGlideManager(mContext).asBitmap().load(url).into(bitmapSimpleTarget);
    }


    @Override
    public void clearMemory() {
        Glide.get(mContext).clearMemory();
    }

    //子线程
    @Override
    public void clearDisk() {
        Glide.get(mContext).clearDiskCache();
    }

    @Override
    public void pause(Context context) {
        getGlideManager(context).pauseRequests();
    }

    @Override
    public void resume(Context context) {
        getGlideManager(context).resumeRequests();
    }

}
