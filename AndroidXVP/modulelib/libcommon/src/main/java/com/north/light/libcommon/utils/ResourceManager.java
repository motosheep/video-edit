package com.north.light.libcommon.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;

import com.north.light.libcommon.R;

import java.io.Serializable;

/**
 * Created by lzt
 * time 2020/12/30
 * 描述：颜色工具类
 */
public class ResourceManager implements Serializable {
    private static Context mContext;

    public static void init(Context context) {
        mContext = context;
    }

    /**
     * 获取颜色
     */
    public static int getColor(int colorId) {
        try {
            return ContextCompat.getColor(mContext.getApplicationContext(), colorId);
        } catch (Exception e) {

        }
        return 0;
    }

    /**
     * 获取drawable
     */
    public static Drawable getDrawable(int drawableId) {
        try {
            return ContextCompat.getDrawable(mContext.getApplicationContext(), drawableId);
        } catch (Exception e) {

        }
        return ContextCompat.getDrawable(mContext.getApplicationContext(), R.mipmap.ic_launcher);
    }

    /**
     * 获取html
     */
    public static Spanned getHtml(String html) {
        if (TextUtils.isEmpty(html)) {
            return Html.fromHtml("");
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(html);
        }
    }
}
