package com.north.light.libcommon.utils;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;


import com.north.light.libcommon.R;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * 状态栏通知工具类
 */
public class NotifycationUtils {
    private Context mContext;
//    private static final String TYPE_CHAT = "chat";
//    private static final String TYPE_PUSH = "push";
    private static final String TYPE_NOTIFY = "notify";
//    private static final int ID_CHAT = 0x1001;
//    private static final int ID_PUSH = 0x2001;
    private static final int ID_NOTIFY = 0x3001;

    //消息通知的map集合
    private ConcurrentMap<String, Integer> mChatNotifyMap = new ConcurrentHashMap<>();

    private static final class SingleHolder {
        static NotifycationUtils mInstance = new NotifycationUtils();
    }

    public static NotifycationUtils getInstance() {
        return SingleHolder.mInstance;
    }

    public void init(Context context) {
        this.mContext = context;
        //创建渠道号
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

//            String channelId1 = TYPE_CHAT;
//            String channelName = "聊天消息";
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            createNotificationChannel(channelId1, channelName, importance);
//
//            String channelId2 = TYPE_PUSH;
//            channelName = "推送消息";
//            importance = NotificationManager.IMPORTANCE_HIGH;
//            createNotificationChannel(channelId2, channelName, importance);

            String channelId3 = TYPE_NOTIFY;
            String channelName = "通知消息";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            createNotificationChannel(channelId3, channelName, importance);
        }
    }

    /**
     * 开播前，结播前时间通知
     */
    public void notifyLiveAuctionTime(String description, String title) {
//        if (TextUtils.isEmpty(description) || mContext == null) return;
//        NotificationManager manager = (NotificationManager) mContext.getSystemService(NOTIFICATION_SERVICE);
//        Notification notification = new NotificationCompat.Builder(mContext, TYPE_NOTIFY)
//                .setContentTitle(title)
//                .setContentText(description)
//                .setWhen(System.currentTimeMillis())
//                .setSmallIcon(R.mipmap.ic_logo_64)
//                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_logo_64))
//                .setAutoCancel(true)
//                .build();
//        manager.notify(ID_NOTIFY, notification);
    }


    @TargetApi(Build.VERSION_CODES.O)
    private void createNotificationChannel(String channelId, String channelName, int importance) {
        NotificationChannel channel = new NotificationChannel(channelId, channelName, importance);
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(
                NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(channel);
    }
}
