package com.ysp.auction.widget.itemdecoration.base

import androidx.recyclerview.widget.RecyclerView

/**
 * author:li
 * date:2020/9/27
 * desc:recycler decoration base class
 */
abstract class BaseDecoration : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {
    private var top: Int = 0
    private  var bottom: Int = 0
    private  var left: Int = 0
    private  var right: Int = 0

    init {
        this.top = top
        this.bottom = bottom
        this.left = left
        this.right = right
    }

    abstract fun top(): Int
    abstract fun bottom(): Int
    abstract fun left(): Int
    abstract fun right(): Int

}