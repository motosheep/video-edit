package com.north.light.libcommon.widget.pic;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lzt
 * on 2019/12/27
 * 多行显示的imageView
 * 通过内嵌imageview实现
 * <p>
 * 实现方法均由外部调用执行
 * 建议把height设置为自适应
 */

public class MulPicView extends LinearLayout {
    private static final String TAG = MulPicView.class.getName();
    private List<ImageView> imageList = new ArrayList<>();
    private OnClickImgListener listener;

    public MulPicView(Context context) {
        this(context, null);
    }

    public MulPicView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MulPicView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    private void init() {
        setOrientation(LinearLayout.VERTICAL);
    }

    /**
     * 根据个数和列数->行数
     * 内嵌LinearLayout实现
     *
     * @param space      间距
     * @param size       数量
     * @param row        每行有多少列
     * @param lineHeight 每一行的高度
     */
    private void inflate(int space, int size, int row, int lineHeight) {
        removeAllViews();
        imageList.clear();
        int lineNum = (size < row) ? 1 : (size / row + ((size > row && size % row > 0) ? 1 : 0));
        List<LinearLayout> mLinearList = new ArrayList<>();
        //初始化LinearLayout
        for (int i = 0; i < lineNum; i++) {
            LinearLayout parentLayout = new LinearLayout(getContext());
            addView(parentLayout);
            LayoutParams linearParams = (LayoutParams) parentLayout.getLayoutParams();
            linearParams.height = lineHeight;
            linearParams.width = LayoutParams.MATCH_PARENT;
            //二级LinearLayout水平对齐
            parentLayout.setOrientation(LinearLayout.HORIZONTAL);
            parentLayout.setLayoutParams(linearParams);
            mLinearList.add(parentLayout);
        }
        //初始化每一个LinearLayout里面的子布局
        for (int i = 0; i < mLinearList.size(); i++) {
            LinearLayout mRootLayout = mLinearList.get(i);
            for (int j = 0; j < row; j++) {
                ImageView childImage = new ImageView(mRootLayout.getContext());
                mRootLayout.addView(childImage);
                LayoutParams imgParams = (LayoutParams) childImage.getLayoutParams();
                imgParams.width = 0;
                imgParams.height = lineHeight;
                imgParams.weight = 1;
                //设置边距
                imgParams.topMargin = space;
                imgParams.bottomMargin = space;
                if (j == 0) {
                    imgParams.rightMargin = space / 2;
                } else if (j == row - 1) {
                    imgParams.leftMargin = space / 2;
                } else {
                    imgParams.leftMargin = space / 2;
                    imgParams.rightMargin = space / 2;
                }
                childImage.setLayoutParams(imgParams);
                imageList.add(childImage);
            }
        }
    }

    /**
     * 设置数据
     */
    public void setUrls(List<String> urlList, int space, int row, int lineHeight, LoadImage callBack) {
        if (urlList == null || urlList.size() == 0) {
            setVisibility(GONE);
        } else {
            setVisibility(VISIBLE);
            //插入布局
            inflate(space, urlList.size(), row, lineHeight);
            for (int i = 0; i < urlList.size(); i++) {
                if (callBack != null) {
                    callBack.load(urlList.get(i), imageList.get(i));
                }
                //设置有图片内容的imageList监听
                final int finalI = i;
                imageList.get(i).setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null) {
                            listener.click(finalI);
                        }
                    }
                });
            }
        }
    }

    public void setOnClickImageListener(OnClickImgListener listener) {
        this.listener = listener;
    }

    public interface OnClickImgListener {
        void click(int pos);
    }

    public interface LoadImage {
        void load(String url, ImageView view);
    }


}
