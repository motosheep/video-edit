package com.north.light.libcommon.toast;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.north.light.libcommon.R;


public class ToastUtils {
    private static Toast mToast;

    //自定义short toast
    public static void toast(Context context, String word, int Length) {
        LayoutInflater inflater = (LayoutInflater) context.getApplicationContext().getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_base_toast, null);
        TextView text = (TextView) view.findViewById(R.id.view_base_toast_tx);
        text.setText(word);
        if (mToast == null) {
            mToast = new Toast(context.getApplicationContext());
        }
        mToast.setGravity(Gravity.BOTTOM, 0, context.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 10);
        mToast.setDuration(Length);
        mToast.setView(view);
        mToast.show();
    }
}
