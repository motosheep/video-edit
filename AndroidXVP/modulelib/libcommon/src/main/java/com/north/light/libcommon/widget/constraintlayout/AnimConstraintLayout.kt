package com.north.light.libcommon.widget.constraintlayout

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout

/**
 * Created by lzt
 * time 2020/12/9
 * 描述：anim constraint layout
 */
class AnimConstraintLayout(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    constructor(context: Context) : this(context, null)

    override fun setVisibility(visibility: Int) {
        val curVis = getVisibility()
        if (visibility != curVis) {
            //如果当前控件可见度没有改变，则不做动画
            isEnabled = false
            //判断显示or隐藏
            if (visibility == View.GONE || visibility == View.INVISIBLE) {
                //显示->隐藏
                this.animate().alpha(0f).setDuration(500).withEndAction {
                    //结束的时候，设置状态
                    isEnabled = true
                    super.setVisibility(visibility)
                }
                return
            } else if (visibility == View.VISIBLE) {
                //隐藏->显示
                this.animate().alpha(1f).setDuration(500).withEndAction {
                    //结束的时候，设置状态
                    isEnabled = true
                    super.setVisibility(visibility)
                }
                return
            }
        }
        isEnabled = true
        super.setVisibility(visibility)
    }
}