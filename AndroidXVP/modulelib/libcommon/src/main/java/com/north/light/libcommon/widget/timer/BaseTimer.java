package com.north.light.libcommon.widget.timer;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;

import androidx.annotation.NonNull;

/**
 * Created by lzt
 * time 2020/9/2
 * 描述：倒数计时基础类
 */
abstract public class BaseTimer extends androidx.appcompat.widget.AppCompatTextView {
    private final int TAG_UPDATE = 0x0001;
    //更新ui handler
    private Handler mCounterHandler;
    //时间戳
    private volatile long mCurTimeStamp = 0L;
    //倒计时时间
    private volatile long mUpdateTime = 1000L;

    public BaseTimer(Context context) {
        this(context, null);
    }

    public BaseTimer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseTimer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (mCounterHandler == null) {
            mCounterHandler = new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(@NonNull Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case TAG_UPDATE:
                            //更新
                            if ((System.currentTimeMillis() - mCurTimeStamp) > 0) {
                                //倒计时时间超过了当前时间
                                end(mCurTimeStamp);
                                this.removeCallbacksAndMessages(null);
                            } else {
                                //还没开始
                                count(mCurTimeStamp);
                                this.sendEmptyMessageDelayed(TAG_UPDATE, mUpdateTime);
                            }
                            break;
                    }
                }
            };
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (mCounterHandler != null) {
            mCounterHandler.removeCallbacksAndMessages(null);
            mCounterHandler.sendEmptyMessage(TAG_UPDATE);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mCounterHandler != null) {
            mCounterHandler.removeCallbacksAndMessages(null);
        }
        super.onDetachedFromWindow();
    }

    //开始计时__注意当前时间的判断
    public void start(long startTime) {
        mCurTimeStamp = startTime;
        if (System.currentTimeMillis() > startTime) {
            end(startTime);
        } else {
            //发送handler
            mCounterHandler.removeCallbacksAndMessages(null);
            mCounterHandler.sendEmptyMessage(TAG_UPDATE);
        }
    }

    /**
     * 设置倒计时时间
     */
    public void setUpdateTime(long time) {
        if (time <= 0) {
            return;
        }
        this.mUpdateTime = time;
    }

    /**
     * 停止计时
     * */
    public void stop(){
        if(mCounterHandler!=null){
            mCounterHandler.removeCallbacksAndMessages(null);
        }
        setText("");
    }

    //抽象方法---------------------------------------------------------------------------------------

    //倒计时结束
    abstract void end(long time);

    //计时中，毫秒时间
    abstract void count(long time);

}
