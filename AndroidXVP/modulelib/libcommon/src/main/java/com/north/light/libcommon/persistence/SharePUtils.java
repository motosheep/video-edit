package com.north.light.libcommon.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import java.util.concurrent.ConcurrentHashMap;


/**
 * share perference utils
 * 原生sp 工具类
 * <p>
 * change by lzt 修改为多对象sharepreference
 * 停止使用
 */
public class SharePUtils {
    private Context mContext;
    //sharepreference集合
    private ConcurrentHashMap<String, SharedPreferences> sharePMap = new ConcurrentHashMap<>();

    private final String DEFAULT_SP_NAME = "SHAREPUTILS_SP_NAME";
    private volatile int SP_MODE = Context.MODE_PRIVATE;

    private volatile boolean isInit = false;

    private static final class SingleHolder {
        static final SharePUtils mInstance = new SharePUtils();
    }

    public static SharePUtils getInstance() {
        return SingleHolder.mInstance;
    }

    //使用前必须初始化
    public void init(Context context) {
        if (context == null) return;
        if (isInit) return;
        this.mContext = context.getApplicationContext();
        isInit = true;
    }

    public synchronized <T> void put(String key, T t) {
        if (TextUtils.isEmpty(key)) return;
        if (t instanceof Integer) {
            getSharePreference(null).edit().putInt(key, (Integer) t).commit();
        } else if (t instanceof Long) {
            getSharePreference(null).edit().putLong(key, (Long) t).commit();
        } else if (t instanceof Boolean) {
            getSharePreference(null).edit().putBoolean(key, (Boolean) t).commit();
        } else if (t instanceof Float) {
            getSharePreference(null).edit().putFloat(key, (Float) t).commit();
        } else if (t instanceof String) {
            getSharePreference(null).edit().putString(key, (String) t).commit();
        }
    }

    public synchronized <T> void put(String spName, String key, T t) {
        if (TextUtils.isEmpty(key)) return;
        if (t instanceof Integer) {
            getSharePreference(spName).edit().putInt(key, (Integer) t).commit();
        } else if (t instanceof Long) {
            getSharePreference(spName).edit().putLong(key, (Long) t).commit();
        } else if (t instanceof Boolean) {
            getSharePreference(spName).edit().putBoolean(key, (Boolean) t).commit();
        } else if (t instanceof Float) {
            getSharePreference(spName).edit().putFloat(key, (Float) t).commit();
        } else if (t instanceof String) {
            getSharePreference(spName).edit().putString(key, (String) t).commit();
        }
    }

    private SharedPreferences getSharePreference(String spName) {
        String name;
        if (spName == null) {
            name = DEFAULT_SP_NAME;
        } else {
            name = spName;
        }
        SharedPreferences spObj = sharePMap.get(name);
        if (spObj == null) {
            spObj = mContext.getSharedPreferences(name, SP_MODE);
            sharePMap.put(name, spObj);
        }
        return spObj;
    }

    public synchronized String getString(String key) {
        if (TextUtils.isEmpty(key)) return null;
        return getSharePreference(null).getString(key, "");
    }

    public synchronized int getInt(String key) {
        if (TextUtils.isEmpty(key)) return -1;
        return getSharePreference(null).getInt(key, -1);
    }

    public synchronized long getLong(String key) {
        if (TextUtils.isEmpty(key)) return -1L;
        return getSharePreference(null).getLong(key, -1L);
    }


    public synchronized float getFloat(String key) {
        if (TextUtils.isEmpty(key)) return -1f;
        return getSharePreference(null).getFloat(key, -1f);
    }

    public synchronized boolean getBoolean(String key) {
        if (TextUtils.isEmpty(key)) return false;
        return getSharePreference(null).getBoolean(key, false);
    }

    //有数据目录名字
    public synchronized String getString(String spName, String key) {
        if (TextUtils.isEmpty(key)) return null;
        return getSharePreference(spName).getString(key, "");
    }

    public synchronized int getInt(String spName, String key) {
        if (TextUtils.isEmpty(key)) return -1;
        return getSharePreference(spName).getInt(key, -1);
    }

    public synchronized long getLong(String spName, String key) {
        if (TextUtils.isEmpty(key)) return -1L;
        return getSharePreference(spName).getLong(key, -1L);
    }


    public synchronized float getFloat(String spName, String key) {
        if (TextUtils.isEmpty(key)) return -1f;
        return getSharePreference(spName).getFloat(key, -1f);
    }

    public synchronized boolean getBoolean(String spName, String key) {
        if (TextUtils.isEmpty(key)) return false;
        return getSharePreference(spName).getBoolean(key, false);
    }


    public synchronized void clear(String spName){
        getSharePreference(spName).edit().clear().commit();
    }


}
