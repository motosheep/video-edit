package com.ysp.auction.widget.itemdecoration.base

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * author:li
 * date:2020/9/27
 * desc:linear vertical decoration
 */
abstract class LinearVerticalDecoration : BaseDecoration() {

    abstract fun startPos(): Int

    override fun getItemOffsets(outRect: Rect, view: View, parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        val position = parent.getChildLayoutPosition(view)
        if (position < startPos()) {
            return
        }
        try {
            curPos(position,outRect)
            outRect.apply {
                if (bottom() != 0) {
                    bottom = bottom()
                }
                if (top() != 0) {
                    top = top()
                }
                if (left() != 0) {
                    left = left()
                }
                if (right() != 0) {
                    right = right()
                }
            }
        } catch (e: Exception) {

        }

    }

    open fun curPos(pos: Int,rect:Rect) {

    }
}