package com.north.light.libcommon.utils;

import android.text.TextUtils;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FormatUtils {

    public static String trainLocCoor(float origin) {
        return new DecimalFormat("#.00000").format(origin);
    }

    /**
     * 补充字符串(两位)
     */
    public static String replaceString(int arg0) {
        DecimalFormat decimalFormat = new DecimalFormat("00");
        String target = decimalFormat.format(arg0);
        return target;
    }


    //时间转换---------------------------
    /*获取现在的时间戳*/
    public static long getNowTimeStamp() {
        return System.currentTimeMillis();
    }

    /*获取昨天的时间戳*/
    public static long getYesterDayTimeStamp() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -1); //向前走一天
        Date date = calendar.getTime();
        return getStringToDate(new SimpleDateFormat("yyyyMMdd").format(date));
    }

    /*获取特定天数的时间戳*/
    public static long getExpecialDayTimeStamp(int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, day);
        return calendar.getTime().getTime();
    }

    /*获取今天的时间戳*/
    public static long getToDayTimeStamp() {
        return getStringToDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
    }

    /*将字符串转为时间戳*/
    public static long getStringToDate(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        try {
            date = sdf.parse(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    /**
     * 把传入的字符串，转换为特定的格式
     */
    public static String getStringBySpecialForm(String time, String form1, String form2) {
        SimpleDateFormat sdf = new SimpleDateFormat(form1);
        Date date = new Date();
        try {
            date = sdf.parse(time);
            return new SimpleDateFormat(form2).format(new Date(date.getTime()));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }


    /*把时间戳转为 年月日 */
    public static String getYMDByTimeStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("yyyy年MM月dd日").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 年 */
    public static String getYByTimeStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("yyyy").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 年月日 */
    public static String getYMDByTimeStamp2(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("yyyy-MM-dd").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 年月日时分 */
    public static String getYMDHMByTimeStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 年月日时分 */
    public static String getYMDHMByTimeStamp2(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("yyyy/MM/dd HH:mm").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 年月日时分秒 */
    public static String getYMDHMSByTimeStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 月日时分 */
    public static String getMDHMByTimeStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("MM/dd HH:mm").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }


    /*把时间戳转为 月日时分 */
    public static String getMDHMByTimeStamp2(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("MM月dd日HH:mm").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 时分秒 */
    public static String getHMSByTimeStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("HH时mm分ss秒").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 分秒 */
    public static String getMSByTimeStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("mm:ss").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /*把时间戳转为 时分 */
    public static String geHMByTimeStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            return new SimpleDateFormat("HH:mm").format(new Date(timeS));
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 获取时间: 11月23日 星期天 晚上 20：30
     */
    public static String getMDWDetailTime(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            String dayMode = getDOrLByStamp(timeStamp);
            String weakDay = getWeakDayByStamp(timeStamp);
            String ymd = new SimpleDateFormat("yyyy年MM月dd日").format(new Date(timeS));
            String hm = new SimpleDateFormat("HH:mm").format(new Date(timeS));
            return ymd + "\t" + weakDay + "\t" + dayMode + "\t" + hm + "\t";
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 传入时间戳判断白天还是晚上
     */
    public static String getDOrLByStamp(String timeStamp) {
        if (TextUtils.isEmpty(timeStamp)) return "";
        try {
            long timeS = Long.parseLong(timeStamp);
            String day = new SimpleDateFormat("HH").format(new Date(timeS));
            int day2 = Integer.parseInt(day);
            if (day2 < 12) {
                return "早上";
            } else if (day2 <= 18) {
                return "下午";
            } else {
                return "晚上";
            }
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * 根据当前时间戳，返回周几
     */
    public static String getWeakDayByStamp(String timeStamp) {
        Calendar cal = CalendarUtils.zeroFromHour(Long.parseLong(timeStamp));
        String name = "";
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
            name = "星期一";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.TUESDAY) {
            name = "星期二";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.WEDNESDAY) {
            name = "星期三";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
            name = "星期四";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            name = "星期五";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
            name = "星期六";
        } else if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            name = "星期日";
        }
        return name;
    }

    /**
     * 返回分秒
     *
     * @return
     */
    public static String calMinSecond(int second) {
        int h = 0;
        int d = 0;
        int s = 0;
        int temp = second % 3600;
        if (second > 3600) {
            h = second / 3600;
            if (temp != 0) {
                if (temp > 60) {
                    d = temp / 60;
                    if (temp % 60 != 0) {
                        s = temp % 60;
                    }
                } else {
                    s = temp;
                }
            }
        } else {
            d = second / 60;
            if (second % 60 != 0) {
                s = second % 60;
            }
        }
        return String.format("%02d", (d + (h * 60))) + "分" + String.format("%02d", s) + "秒";
    }

    /**
     * 返回日时分秒
     *
     * @return
     */
    public static String calHourMinSecond(int second) {
        int h = 0;
        int d = 0;
        int s = 0;
        int temp = second % 3600;
        if (second > 3600) {
            h = second / 3600;
            if (temp != 0) {
                if (temp > 60) {
                    d = temp / 60;
                    if (temp % 60 != 0) {
                        s = temp % 60;
                    }
                } else {
                    s = temp;
                }
            }
        } else {
            d = second / 60;
            if (second % 60 != 0) {
                s = second % 60;
            }
        }
        return String.format("%02d", h) + "时" + String.format("%02d", d) + "分" + String.format("%02d", s) + "秒";
    }

    /**
     * 返回日时分秒
     *
     * @return
     */
    public static String calHourMin(int second) {
        int h = 0;
        int d = 0;
        int temp = second % 3600;
        if (second > 3600) {
            h = second / 3600;
            if (temp != 0) {
                if (temp > 60) {
                    d = temp / 60;
                }
            }
        } else {
            d = second / 60;
        }
        return String.format("%02d", h) + "时" + String.format("%02d", d) + "分";
    }
}
