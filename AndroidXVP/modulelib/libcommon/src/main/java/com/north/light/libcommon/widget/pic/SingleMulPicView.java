package com.north.light.libcommon.widget.pic;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.north.light.libcommon.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lzt
 * on 2019/12/26
 * 单行，多图片
 * 显示多张图片的view（小于等于三张）
 * 注意，若在reycclerview中使用该控件，需要关注控件的复用和生命周期的回调
 */

public class SingleMulPicView extends LinearLayout {
    private static final String TAG = SingleMulPicView.class.getName();
    private List<String> picUrls = new ArrayList<>();
    private List<ImageView> viewList = new ArrayList<>();
    private List<TextView> txList = new ArrayList<>();

    public SingleMulPicView(Context context) {
        this(context, null);
    }

    public SingleMulPicView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SingleMulPicView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    /**
     * adapter中必须调用此方法
     */
    private void inflate(int size, int space) {
        int finalSize = Math.min(size, 3);
        removeAllViews();
        setOrientation(LinearLayout.HORIZONTAL);
        viewList.clear();
        txList.clear();
        for (int i = 0; i < finalSize; i++) {
            RelativeLayout picRelative = new RelativeLayout(getContext());
            addView(picRelative);
            LayoutParams relParams = (LayoutParams) picRelative.getLayoutParams();
            relParams.weight = 1;
            relParams.height = LayoutParams.MATCH_PARENT;
            relParams.width = 0;
            if (i == 0) {
                relParams.rightMargin = space;
            } else if (i == 1) {
                relParams.leftMargin = space;
                relParams.rightMargin = space;
            } else {
                relParams.leftMargin = space;
            }
            picRelative.setLayoutParams(relParams);
            //添加imageview
            ImageView picView = new ImageView(getContext());
            picView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            picRelative.addView(picView);
            RelativeLayout.LayoutParams picParams = (RelativeLayout.LayoutParams) picView.getLayoutParams();
            picParams.height = LayoutParams.MATCH_PARENT;
            picParams.width = LayoutParams.MATCH_PARENT;
            picView.setLayoutParams(picParams);
            viewList.add(picView);
            //添加文字控件
            TextView txView = new TextView(getContext());
            picRelative.addView(txView);
            txView.setGravity(Gravity.CENTER);
            txView.setTextSize(12);
            txView.setTextColor(getContext().getResources().getColor(R.color.color_FFFFFF));
            txView.setBackgroundResource(R.color.color_99000000);
            RelativeLayout.LayoutParams txParams = (RelativeLayout.LayoutParams) txView.getLayoutParams();
            txParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            txParams.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            txParams.height = RelativeLayout.LayoutParams.WRAP_CONTENT;
            txView.setLayoutParams(txParams);
            txView.setPadding(0, 10, 0, 10);
            txList.add(txView);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    //设置url
    public void setUrls(List<String> picUrls, LoadImage loadImage) {
        inflate(picUrls.size(),10);
        this.picUrls = (picUrls == null ? new ArrayList() : picUrls);
        for (int i = 0; i < (Math.min(this.picUrls.size(), 3)); i++) {
            if (loadImage != null) {
                loadImage.load(this.picUrls.get(i), viewList.get(i), txList.get(i), i);
            }
        }
    }


    public interface LoadImage {
        void load(String url, ImageView img, TextView tx, int pos);
    }
}
