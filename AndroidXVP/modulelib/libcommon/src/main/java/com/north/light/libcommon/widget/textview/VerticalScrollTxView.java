package com.north.light.libcommon.widget.textview;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;


import com.north.light.libcommon.R;
import com.north.light.libcommon.log.KtLogUtil;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by lzt
 * time 2020/6/9
 * 描述：竖直滚动文字text view
 */
public class VerticalScrollTxView extends TextSwitcher implements TextSwitcher.ViewFactory {
    private Context mContext;
    //handler
    private Handler mUIHandler;
    //文字大小
    private float mTextSize = 12f;
    //文字颜色__默认为0
    private int mTextColor = 0;
    //显示文字的集合
    private CopyOnWriteArrayList<String> mShowList = new CopyOnWriteArrayList<>();
    //handler 标识
    private static final int TAG_SHOW = 0x0002;
    private static final int TAG_SHOW_EMPTY = 0x0003;
    //滚动时间间隔
    private long mScrollInterval = 2000;
    //启动标识
    private volatile boolean mRunningTAG = false;

    public VerticalScrollTxView(Context context) {
        this(context, null);
    }

    public VerticalScrollTxView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initView();
    }

    /**
     * 初始化view
     */
    private void initView() {
        // 设置转换动画，这里引用系统自带动画
        Animation in = AnimationUtils.loadAnimation(mContext, R.anim.anim_scroll_in);
        Animation out = AnimationUtils.loadAnimation(mContext, R.anim.anim_scroll_out);
        this.setInAnimation(in);
        this.setOutAnimation(out);
        //设置ViewSwitcher.ViewFactory
        this.setFactory(this);

        //线程初始化
        if (mUIHandler == null) {
            mUIHandler = new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case TAG_SHOW:
                            //展示
                            if (mShowList != null && mShowList.size() != 0) {
                                try {
                                    TextView content = ((TextView) (VerticalScrollTxView.this.getNextView()));
                                    content.setGravity(Gravity.CENTER);
                                    content.setMaxLines(1);
                                    content.setTextSize(mTextSize);
                                    content.setTextColor(getResources().getColor(R.color.color_FFFFFF));
                                    content.setShadowLayer(6,6,6,R.color.color_000000);
                                    //设置宽高
                                    FrameLayout.LayoutParams params = (LayoutParams) content.getLayoutParams();
                                    params.height = LayoutParams.MATCH_PARENT;
                                    params.width = LayoutParams.MATCH_PARENT;
                                    content.setLayoutParams(params);
                                } catch (Exception e) {
                                    KtLogUtil.d("滚动文字控件设置错误");
                                }
                                setText(mShowList.get(0));
                                mShowList.remove(0);
                                this.sendEmptyMessageDelayed(TAG_SHOW, mScrollInterval);
                            } else {
                                //3s后发送隐藏布局msg
                                this.sendEmptyMessage(TAG_SHOW_EMPTY);
                            }
                            break;
                        case TAG_SHOW_EMPTY:
                            //展示完成，隐藏布局
                            this.removeCallbacksAndMessages(null);
                            setText("");
                            mRunningTAG = false;
                            break;
                    }
                }
            };
        }
    }

    /**
     * 从窗口销毁
     */
    @Override
    protected void onDetachedFromWindow() {
        if (mUIHandler != null) {
            mUIHandler.removeCallbacksAndMessages(null);
        }
        super.onDetachedFromWindow();
    }


    @Override
    public View makeView() {
        TextView textView = new TextView(mContext);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(mTextSize);
        if (mTextColor != 0) {
            textView.setTextColor(mTextColor);
        }
        return textView;
    }

    //外部调用函数---------------------------------------------------------------


    public void setTextSize(float mTextSize) {
        this.mTextSize = mTextSize;
    }

    /**
     * 设置滚动时间间隔
     */
    public void setScrollInterval(long interval) {
        this.mScrollInterval = interval;
    }

    /**
     * 文字集合
     */
    public void setTextList(List<String> txList) {
        if (this.mShowList == null) {
            this.mShowList = new CopyOnWriteArrayList<>();
        }
        mShowList.addAll(txList);
        if (mUIHandler != null) {
            if (mRunningTAG) {
                return;
            }
            mRunningTAG = true;
            mUIHandler.sendEmptyMessage(TAG_SHOW);
        }
    }
}
