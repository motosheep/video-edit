package com.north.light.libcommon.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;


/**
 * create by lzt
 * data 2019/12/17
 */
public abstract class BaseDialog extends Dialog {
    private View mRootView;

    public BaseDialog(@NonNull Context context) {
        super(context);
        initView();
        init();
    }


    private void initView() {
        mRootView = LayoutInflater.from(getContext()).inflate(getLayoutId(), null);
        setContentView(mRootView);
        // 设置宽度为屏宽, 靠近屏幕底部。
        Window win = getWindow();
        // 一定要设置Background，如果不设置，window属性设置无效
        win.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams params = win.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.width = (setWidth() == 0 ? WindowManager.LayoutParams.WRAP_CONTENT  : setWidth());
        params.height = (setHeight() == 0 ? WindowManager.LayoutParams.WRAP_CONTENT : setHeight());
        win.setAttributes(params);
    }

    public abstract void init();

    public View getRootView() {
        return mRootView;
    }

    public void show() {
        if (!isShowing())
            BaseDialog.super.show();
    }

    public void dismiss() {
        try {
            super.dismiss();
        } catch (Exception e) {

        }
    }

    public abstract int getLayoutId();

    public abstract int setWidth();

    public abstract int setHeight();


}
