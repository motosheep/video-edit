package com.north.light.libcommon.exception;


/**
 * 全部错误捕捉器
 * //设置全部异常捕获
 * try {
 * MyCatchException handler = MyCatchException.getInstance();
 * Thread.setDefaultUncaughtExceptionHandler(handler);
 * KtLogUtil.d("init----设置全局自定义异常捕获成功");
 * } catch (Exception e) {
 * KtLogUtil.d("init----设置全局自定义异常捕获错误: " + e.getMessage());
 * }
 */
public class MyCatchException implements Thread.UncaughtExceptionHandler {
    /**
     * MyCatchException实例
     */

    private static MyCatchException instance;

    /**
     * 获取MyCatchException实例 ,单例模式
     */

    public static MyCatchException getInstance() {
        if (instance == null) {
            instance = new MyCatchException();
        }
        return instance;
    }

    /**
     * 异常捕获
     */
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        System.out.println("全局异常捕获--start---");
        //如果全局异常，则清空用户登录信息，为的是用户下次进入，可以在的登录页面重新登录，准备新应用的下发
        try {
            //清除sp数据，让用户重新登录
//            MMKVManager.getInstance().clear(SpConstants.SP_USER_INFO);
            Thread.sleep(2000);
        } catch (Exception e1) {
            System.out.println("清空用户信息异常: " + e1.getMessage());
        }
        android.os.Process.killProcess(android.os.Process.myPid());
        System.out.println("全局异常捕获--end---");
    }
}