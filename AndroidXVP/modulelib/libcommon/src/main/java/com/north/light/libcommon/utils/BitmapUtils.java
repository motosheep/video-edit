package com.north.light.libcommon.utils;

import android.graphics.Bitmap;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by lzt
 * time 2020/3/30
 * 描述：bitmap工具类
 */
public class BitmapUtils {

    private static final class SingleHolder {
        static final BitmapUtils mInstance = new BitmapUtils();
    }

    public static BitmapUtils getInstance() {
        return SingleHolder.mInstance;
    }


    /**
     * bitmap保存为本地图片
     *
     * @param bitmap       原图片
     * @param path         保存路径
     * @param filename     图片名字
     * @param compressRate 压缩比率
     */
    private void saveBitmap(Bitmap bitmap, String path, String filename, int compressRate) throws Exception {
        File file = new File(path + filename);
        if (file.exists()) {
            file.delete();
        }
        FileOutputStream out;
        out = new FileOutputStream(file);
        if (bitmap.compress(Bitmap.CompressFormat.PNG, compressRate, out)) {
            out.flush();
            out.close();
        }
    }
}
