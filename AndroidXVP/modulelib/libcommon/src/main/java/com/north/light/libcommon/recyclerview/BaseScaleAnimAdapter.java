package com.north.light.libcommon.recyclerview;

import android.animation.Animator;
import android.content.Context;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


public abstract class BaseScaleAnimAdapter<W extends Object, T extends RecyclerView.ViewHolder> extends BaseSimpleAdapter<W, T> {
    private ScaleInAnimation mSelectAnimation = new ScaleInAnimation();

    public BaseScaleAnimAdapter(Context context) {
        super(context);
    }

    private void addAnimation(T holder) {
        for (Animator anim : mSelectAnimation.getAnimators(holder.itemView)) {
            anim.setDuration(300).start();
            anim.setInterpolator(new LinearInterpolator());
        }
    }

    @Override
    public void onViewAttachedToWindow(@NonNull T holder) {
        super.onViewAttachedToWindow(holder);
        addAnimation(holder);
    }




}
