package com.north.light.libcommon.utils;

import android.text.TextUtils;

import com.north.light.libcommon.log.KtLogUtil;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by lzt
 * time 2020/8/14
 * 描述：md5工具类
 */
public class MD5Utils implements Serializable {

    private static final String TAG = MD5Utils.class.getSimpleName();

    /**
     * 字符串转md5
     */
    public static String stringToMD5(String plainText) {
        if (TextUtils.isEmpty(plainText)) {
            KtLogUtil.d(TAG, "MD5加密传入字符串为空");
            return "";
        }
        byte[] secretBytes = null;
        try {
            secretBytes = MessageDigest.getInstance("md5").digest(
                    plainText.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有这个md5算法！");
        }
        String md5code = new BigInteger(1, secretBytes).toString(16);
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code = "0" + md5code;
        }
        return md5code;
    }
}
