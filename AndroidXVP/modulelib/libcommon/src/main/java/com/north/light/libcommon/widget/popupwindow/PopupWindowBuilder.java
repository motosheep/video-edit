package com.north.light.libcommon.widget.popupwindow;

import android.widget.PopupWindow;

import java.io.Serializable;

/**
 * Created by lzt
 * time 2021/1/28
 * 描述：pop window构建类
 * builder 模式
 */
public class PopupWindowBuilder implements Serializable {

    private static final class SingleHolder {
        public static PopupWindowBuilder mInstance = new PopupWindowBuilder();
    }

    public static PopupWindowBuilder getInstance() {
        return SingleHolder.mInstance;
    }

    /**
     * 构建popup参数
     */
    public <T extends PopupWindow> T initPopupWindow(T t, Builder builder) {
        t.setFocusable(builder.isFocusable);
        t.setClippingEnabled(builder.canClickOutside);
        t.setElevation(builder.elevation);
        t.setTouchable(builder.touchable);
        t.setClippingEnabled(builder.clippingEnable);
        return t;
    }

    /**
     * builder构造类
     */
    public static class Builder implements Serializable {
        //focusable--按下返回键后，是否退出当前popup window
        private boolean isFocusable = true;
        //外部区域是否可以点击取消
        private boolean canClickOutside = true;
        //弹出动画高度--默认100
        private int elevation = 100;
        //是否可以触摸
        private boolean touchable = true;
        //允许弹出窗口扩展到屏幕范围之外
        private boolean clippingEnable = false;

        public Builder setCanClickOutside(boolean canClickOutside) {
            this.canClickOutside = canClickOutside;
            return this;
        }

        public Builder setFocusable(boolean focusable) {
            this.isFocusable = focusable;
            return this;
        }

        public Builder setElevation(int elevation) {
            this.elevation = elevation;
            return this;
        }

        public Builder setTouchable(boolean touchable) {
            this.touchable = touchable;
            return this;
        }

        public Builder setClippingEnable(boolean clippingEnable) {
            this.clippingEnable = clippingEnable;
            return this;
        }
    }

}
