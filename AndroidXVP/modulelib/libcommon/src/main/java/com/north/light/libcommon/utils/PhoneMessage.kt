package com.north.light.libcommon.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.telephony.TelephonyManager
import android.text.TextUtils
import android.util.DisplayMetrics

import java.lang.reflect.Method

import android.content.Context.TELEPHONY_SERVICE

/**
 * 手机信息类
 * imei 设备名
 */
class PhoneMessage {
    @Volatile
    private var IMEI = "000000000000001"//默认全是0
    @Volatile
    var brand = ""
        private set//品牌
    @Volatile
    var device = ""
        private set//设备名
    @Volatile
    var model = ""
        private set//用户可见名称
    @Volatile
    var phonE_WIDTH = 0
        private set//宽
    @Volatile
    var phonE_HEIGHT = 0
        private set//高
    private var mTelephonyManager: TelephonyManager? = null

    val imei: String
        get() {
            if (TextUtils.isEmpty(IMEI)) {
                IMEI = "000000000000001"
            }
            return IMEI
        }


    private object SingleHolder {
        internal var mInstance = PhoneMessage()
    }

    /**
     * 外部获取所有的权限后，调用此方法获取手机相关的信息
     */
    @SuppressLint("MissingPermission")
    fun init(context: Context?) {
        if (context == null) {
            return
        }
        if (phonE_WIDTH != 0) {
            return
        }
        try {
            if (mTelephonyManager == null) {
                mTelephonyManager = context.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
            }
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                IMEI = mTelephonyManager!!.deviceId
            } else {
                val method = mTelephonyManager!!.javaClass.getMethod("getImei")
                IMEI = method.invoke(mTelephonyManager) as String
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        brand = Build.BRAND
        device = Build.DEVICE
        model = Build.MODEL
        val dm = context.resources.displayMetrics
        phonE_WIDTH = dm.widthPixels
        phonE_HEIGHT = dm.heightPixels
    }

    companion object {
        private val TAG = PhoneMessage::class.java.name

        val instance: PhoneMessage
            get() = SingleHolder.mInstance
    }
}
