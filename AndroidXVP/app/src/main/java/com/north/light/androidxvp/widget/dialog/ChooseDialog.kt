//package com.north.light.androidxvp.widget.dialog
//
//import android.content.Context
//import com.north.light.androidxvp.R
//import com.north.light.libcommon.utils.PhoneMessage
//import com.north.light.libcommon.widget.dialog.BaseDialog
//import kotlinx.android.synthetic.main.dialog_choose.*
//
///**
// * Created by lzt
// * time 2020/8/17
// * 描述：选择dialog__取消,确定
// * change by lzt 20200929 增加按钮ui修改逻辑
// */
//class ChooseDialog(context: Context) : BaseDialog(context) {
//    //监听
//    var mListener: OnClickListener? = null
//    //传参
//    var mArg: String? = null
//    //cancel字体
//    var mCancelText = "取消"
//    //确定字体
//    var mConfirmText = "确定"
//
//    override fun init() {
//        dialog_choose_cancel.setOnClickListener {
//            //取消
//            dismiss()
//            this.mListener?.cancel(mArg ?: "")
//        }
//        dialog_choose_confirm.setOnClickListener {
//            //确定
//            dismiss()
//            this.mListener?.confirm(mArg ?: "")
//        }
//    }
//
//    override fun getLayoutId(): Int {
//        return R.layout.dialog_choose
//    }
//
//    override fun setWidth(): Int {
//        return PhoneMessage.instance.phonE_WIDTH
//    }
//
//    override fun setHeight(): Int {
//        return PhoneMessage.instance.phonE_HEIGHT
//    }
//
//    //设置标题
//    fun show(title: String, arg: String) {
//        super.show()
//        this.mArg = arg
//        dialog_choose_title.text = title
//    }
//
//    //设置标题
//    fun show(title: String, arg: String, cancelTx: String, confirm: String) {
//        super.show()
//        this.mArg = arg
//        dialog_choose_title.text = title
//        mCancelText = cancelTx
//        mConfirmText = confirm
//        dialog_choose_cancel.text = mCancelText
//        dialog_choose_confirm.text = mConfirmText
//    }
//
//    //监听
//    fun setOnClickListener(mListener: OnClickListener) {
//        this.mListener = mListener
//    }
//
//    fun removeClickListener() {
//        this.mListener = null
//    }
//
//    interface OnClickListener {
//        fun confirm(arg: String)
//
//        fun cancel(arg: String)
//    }
//}