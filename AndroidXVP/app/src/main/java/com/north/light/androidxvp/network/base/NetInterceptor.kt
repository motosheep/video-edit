package com.north.light.androidxvp.network.base

import com.north.light.libnetwork.interceptor.TokenInterceptor

import okhttp3.Interceptor
import okhttp3.Request

/**
 * create by lzt
 * data 2019/12/21
 */
class NetInterceptor : TokenInterceptor() {

    override fun getRequest(chain: Interceptor.Chain): Request {
        val original = chain.request()
        //添加请求参数
        val url = original.url().newBuilder()
                .build()
        //添加请求头
        //        String head = LoginRecord.Companion.getInstance().getToken();
        //        String phone = LoginRecord.Companion.getInstance().getPhone();
        //        LogUtil.d(TAG, "请求的token: " + head);
        return original.newBuilder()
                //                .addHeader("Content-Type", "application/json")
                .addHeader("Connection", "keep-alive")
                //                .addHeader("token", TextUtils.isEmpty(head) ? "" : head)
                //                .addHeader("uuid", TextUtils.isEmpty(phone) ? "" : phone)
                .method(original.method(), original.body())
                .url(url)
                .build()
    }

    companion object {
        private val TAG = NetInterceptor::class.java.name
    }
}
