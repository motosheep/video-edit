package com.north.light.androidxvp.network.base

import com.north.light.androidxvp.network.bean.BaseResult
import io.reactivex.rxjava3.observers.DisposableObserver


class NetDisposableSubscribe<T : BaseResult<*>> : DisposableObserver<T>() {


    override fun onStart() {
        super.onStart()
    }

    override fun onNext(value: T) {

    }

    override fun onError(e: Throwable) {
        if (e is TokenException) {
            //需要重新登录__时间差判断
            //            if (System.currentTimeMillis() - NetConstants.mLoginShowInterval > 2000) {
            //                NetConstants.mLoginShowInterval = System.currentTimeMillis();
            //                Toast.makeText(EbayApplication.getContext(), "账号信息过期，需要重新登录", Toast.LENGTH_SHORT).show();
            //                Intent mIntent = new Intent();
            //                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //                mIntent.setClassName(EbayApplication.getContext().getPackageName(), LoginActivity.class.getName());
            //                EbayApplication.getContext().startActivity(mIntent);
            //            }
        }
    }

    override fun onComplete() {

    }
}
