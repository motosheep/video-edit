package com.north.light.androidxvp.ui.welcome

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.north.light.androidxvp.R
import com.north.light.androidxvp.protect.AppStatus
import com.north.light.androidxvp.protect.AppStatusManager
import com.north.light.androidxvp.ui.main.MainActivity
import com.north.light.libmvp.mvp.BaseNoPActivity

class WelcomeActivity : BaseNoPActivity() {
    private var mDelayHandler: Handler? = null

    override fun getLayout(): Int {
        return R.layout.activity_welcome
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //防止程序安装器打开app和Launcher打开app导致的app重启问题
        if (intent.flags and Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT != 0) {
            finish()
            return
        }
        AppStatusManager.instance.mAppStatus = AppStatus.STATUS_NORMAL
        mDelayHandler = Handler(Looper.getMainLooper())

    }

    override fun onStart() {
        super.onStart()
        mDelayHandler?.postDelayed(Runnable {
            finish()
            MainActivity.launch(this, 0)
        }, 1000)
    }

    override fun onStop() {
        mDelayHandler?.removeCallbacksAndMessages(null)
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
