package com.north.light.androidxvp.base

import com.north.light.libmvp.mvp.base.BaseMvpPresenter
import com.north.light.libmvp.mvp.base.IMvpBaseView

/**
 * Created by lzt
 * time 2020/5/14
 * 描述：
 */
 abstract class BasePresenter<V : IMvpBaseView?> : BaseMvpPresenter<V>() {
}