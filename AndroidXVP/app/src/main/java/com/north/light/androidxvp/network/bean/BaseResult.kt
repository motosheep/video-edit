package com.north.light.androidxvp.network.bean

import com.north.light.libnetwork.bean.BaseCode

/**
 * create by lzt
 * data 2019/12/21
 */
class BaseResult<T> : BaseCode() {
    var data: T? = null
}
