package com.north.light.androidxvp.protect

/**
 * Created by lzt
 * time 2020/6/3
 * 描述：应用状态管理类
 */
class AppStatusManager {
    var mAppStatus: Int = AppStatus.STATUS_FORCE_KILLED

    companion object {
        val instance = SingletonHolder.holder
    }

    private object SingletonHolder {
        val holder = AppStatusManager()
    }


}