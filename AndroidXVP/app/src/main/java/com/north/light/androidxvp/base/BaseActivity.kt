package com.north.light.androidxvp.base

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import com.north.light.androidxvp.R
import com.north.light.androidxvp.protect.AppStatus
import com.north.light.androidxvp.protect.AppStatusManager
import com.north.light.androidxvp.ui.main.MainActivity
import com.north.light.androidxvp.widget.dialog.LoadingDialog
import com.north.light.libmvp.mvp.BaseSteepActivity
import com.north.light.libmvp.mvp.base.IMvpBasePresenter
import com.north.light.libmvp.mvp.base.StatusBarUtils

/**
 * Created by lzt
 * time 2020/5/14
 * 描述：base activity 基类
 */
abstract class BaseActivity<T : IMvpBasePresenter<*>?> : BaseSteepActivity<T>() {
    private var mDialog: LoadingDialog? = null
    private var mInputManager: InputMethodManager? = null
    open var TAG: String = javaClass.simpleName.toString()

    override fun statusColor(): Int {
        return R.color.color_000000
    }

    override fun backgroundRes(): Int {
        return R.color.color_00000000
    }

    override fun isNeedStatus(): Boolean {
        return false
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //判断app状态__回收则重启
        when {
            AppStatusManager.instance.mAppStatus == AppStatus.STATUS_FORCE_KILLED -> {
                //异常状态(被回收了)：跳到主页,主页launch mode single task
                var intent = Intent(this, MainActivity::class.java)
                intent.putExtra(AppStatus.KEY_HOME_ACTION, AppStatus.ACTION_RESTART_APP)
                startActivity(intent)
            }
            AppStatusManager.instance.mAppStatus == AppStatus.STATUS_NORMAL -> {
                //正常状态
                mInputManager = applicationContext.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                try {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)//竖屏
                } catch (e: Exception) {

                }
                setThemeTxColor()
                initData()
            }
        }
    }

    abstract fun initData()

    override fun onDestroy() {
        mInputManager = null
        dismissLoading()
        super.onDestroy()
    }


    override fun showLoading() {
        super.showLoading()
        if (mDialog == null) {
            mDialog = LoadingDialog(this);
        }
        //不空则show
        mDialog?.show()
    }

    override fun dismissLoading() {
        super.dismissLoading()
        mDialog?.dismiss()
    }

    open fun setThemeTxColor() {
        //黑色字体
        StatusBarUtils.setStatusTextColor(this, true)
    }
}