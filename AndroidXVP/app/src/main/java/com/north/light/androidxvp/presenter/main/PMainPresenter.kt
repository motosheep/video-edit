package com.north.light.androidxvp.presenter.main

import com.north.light.androidxvp.base.BasePresenter
import com.north.light.androidxvp.ui.main.MainActivity

/**
 * Created by lzt
 * time 2020/12/2
 * 描述：
 */
class PMainPresenter : BasePresenter<MainActivity>()
