package com.north.light.androidxvp.widget.dialog

import android.content.Context
import com.north.light.androidxvp.R
import com.north.light.libcommon.utils.PhoneMessage
import com.north.light.libcommon.widget.dialog.BaseDialog

/**
 * Created by lzt
 * time 2020/5/14
 * 描述：
 */
open class LoadingDialog(context: Context) : BaseDialog(context) {

    override fun init() {
    }

    override fun getLayoutId(): Int {
        return R.layout.dialog_loading
    }

    override fun setWidth(): Int {
        return PhoneMessage.instance.phonE_WIDTH
    }

    override fun setHeight(): Int {
        return PhoneMessage.instance.phonE_HEIGHT
    }
}