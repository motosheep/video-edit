package com.north.light.androidxvp.base

import com.north.light.libmvp.mvp.base.IMvpBasePresenter
import com.north.light.libmvp.mvp.base.StatusBarUtils

/**
 * Created by lzt
 * time 2021/1/12
 * 描述：
 */
abstract class BaseStatusActivity<T : IMvpBasePresenter<*>?> : BaseActivity<T>() {

    override fun setThemeTxColor() {
        when (getThemeTxColor()) {
            1 -> StatusBarUtils.setStatusTextColor(this, true)
            2 -> StatusBarUtils.setStatusTextColor(this, false)
            else -> StatusBarUtils.setStatusTextColor(this, true)
        }
    }

    //获取状态栏的字体颜色:0默认 1黑色 2白色
    open fun getThemeTxColor(): Int {
        return 2
    }

    override fun initData() {

    }
}