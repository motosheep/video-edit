package com.north.light.androidxvp.base

import android.os.Bundle
import com.north.light.androidxvp.widget.dialog.LoadingDialog
import com.north.light.libcommon.log.KtLogUtil
import com.north.light.libmvp.mvp.BaseFragment
import com.north.light.libmvp.mvp.base.IMvpBasePresenter
import java.lang.Exception

/**
 * Created by lzt
 * time 2020/5/14
 * 描述：
 */
abstract class BaseFragment<T : IMvpBasePresenter<*>?> : BaseFragment<T>() {

    override fun initData() {
        super.initData()
    }

    private var mDialog: LoadingDialog? = null
    open var TAG: String = javaClass.simpleName.toString()

    override fun showLoading() {
        try {
            if (mDialog == null) {
                mDialog = LoadingDialog(activity)
            }
            //不空则show
            mDialog?.show()
        } catch (e: Exception) {
            KtLogUtil.d(TAG,"show loading error: " + e.message)
        }
    }

    override fun dismissLoading() {
        mDialog?.dismiss()
    }



}