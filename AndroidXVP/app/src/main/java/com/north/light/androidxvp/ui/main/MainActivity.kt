package com.north.light.androidxvp.ui.main

import android.app.Activity
import android.content.Intent
import com.marvhong.videoeditor.VideoEditFun
import com.north.light.androidxvp.R
import com.north.light.androidxvp.base.BaseActivity
import com.north.light.androidxvp.presenter.main.PMainPresenter
import com.north.light.androidxvp.protect.AppStatus
import com.north.light.androidxvp.ui.WaterActivity
import com.north.light.androidxvp.ui.welcome.WelcomeActivity
import com.north.light.libmvp.router.XRouter

class MainActivity : BaseActivity<PMainPresenter>() {

    companion object {
        //0默认启动
        val DATA_TYPE = "DATA_TYPE"

        fun launch(from: Activity?, type: Int) {
            XRouter.getInstance().newRouter()
                .putInt(DATA_TYPE, type)
                .launch(from, MainActivity::class.java)
        }
    }

    override fun createPresenter(): PMainPresenter {
        return PMainPresenter()
    }

    override fun getLayout(): Int {
        return R.layout.activity_main
    }

    override fun initData() {
        VideoEditFun.getInstance().init(this)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    /**
     * 启动已经存在的activity,该方法会执行
     * */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        //改activity启动模式为single task，需要set intent
        //再次启动activity时，若为后台清空activity状态，则直接打开启动页面，重新走启动流程，防止空指针行为
        setIntent(intent)
        var action = intent?.getIntExtra(AppStatus.KEY_HOME_ACTION, AppStatus.ACTION_BACK_TO_HOME)
        if (action == AppStatus.ACTION_RESTART_APP) {
            startActivity(Intent(this, WelcomeActivity::class.java))
            finish()
            return
        }
    }


}
