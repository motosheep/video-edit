package com.north.light.androidxvp.protect

/**
 * Created by lzt
 * time 2020/6/3
 * 描述：应用状态
 */
object AppStatus {
    var STATUS_FORCE_KILLED = -1 //应用放在后台被强杀了__默认初始值
    var STATUS_NORMAL = 2 //APP正常态__正常启动prepare activity时，设置该值
    var KEY_HOME_ACTION = "key_home_action"//返回到主页面
    var ACTION_BACK_TO_HOME = 6 //默认值
    var ACTION_RESTART_APP = 9//被强杀

}