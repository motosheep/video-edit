package com.north.light.androidxvp.network.bean

import java.util.ArrayList

/**
 * Created by lzt
 * time 2020/5/22
 * 描述：base page分页
 */
class BasePage<T> {
    var records: List<T> = ArrayList()
    //当前页数
    var current: Long = 0
    //总页数
    var total: Long = 0
}
