package com.north.light.androidxvp.constant

import com.north.light.androidxvp.BuildConfig

object NetConstants {
    private val isDebug = BuildConfig.DEBUG
    val BASE_IP = if (isDebug) "192.168.168.104" else "134.175.219.96"
    //软件服务器url
    val BASE_URL = if (isDebug)
        "http://192.168.168.104:9901/ebay/"
    else
        "http://motosheep.xyz:9901/ebay/"

    @Volatile
    var mLoginShowInterval: Long = 0//登录页面弹出的时间间隔
    val CODE_TOKEN_ERROR = 402
    val CODE_TOKEN_ERROR2 = 401
    val CODE_SUCCESS = 200
}
