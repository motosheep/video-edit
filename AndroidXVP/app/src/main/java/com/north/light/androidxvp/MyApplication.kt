package com.north.light.androidxvp

import android.content.Context
import android.os.Environment
import android.widget.ImageView
import androidx.multidex.MultiDexApplication

import com.north.light.androidxvp.constant.Constant
import com.north.light.libcommon.persistence.MMKVManager
import com.north.light.libcommon.utils.PhoneMessage
import com.north.light.libcommon.utils.ProcessUtil
import com.north.light.libloadpic.ImageManager
import com.north.light.libpicselect.model.PicSelConfig
import com.tencent.bugly.Bugly
import com.tencent.bugly.beta.Beta

/**
 * Created by lzt
 * time 2020/12/2
 * 描述：
 */
class MyApplication : MultiDexApplication() {

    companion object{
        private lateinit var mContext: Context
        @JvmStatic
        fun getContext(): Context {
            return mContext
        }
    }


    override fun onCreate() {
        super.onCreate()
        if (ProcessUtil.getCurrentProcessName(this) == packageName) {
            mContext = this.applicationContext
            initBugly()
            MMKVManager.getInstance().init("NORTH_LIGHT_DB", this)
            ImageManager.getInstance().init(this)
            PhoneMessage.instance.init(this)
            //图片加载库__加载小图片的时候，分辨率调整为200*200
            PicSelConfig.getInstance().setLoaderManager(this, object : PicSelConfig.BindImageViewListener {
                override fun BindImageView(path: String, iv: ImageView) {
                    ImageManager.getInstance().loadNormalPic(path, iv)
                }

                override fun BindSmallImageView(path: String, iv: ImageView) {
                    ImageManager.getInstance().loadNormalPicBySize(200, 200, path, iv, R.mipmap.ic_load_pic_default_logo)
                }
            })
        }
    }


    /**
     * bugly init
     */
    private fun initBugly() {
        Beta.autoInit = true
        Beta.autoCheckUpgrade = true
        Beta.upgradeCheckPeriod = (60 * 1000 * 2).toLong()
        Beta.initDelay = (6 * 1000).toLong()
        Beta.largeIconId = R.mipmap.ic_launcher
        Beta.smallIconId = R.mipmap.ic_launcher
        Beta.defaultBannerId = R.mipmap.ic_launcher
        Beta.storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        Beta.showInterruptedStrategy = true
        Bugly.init(applicationContext, Constant.CONSTANT_BUGLY_ID, false)
    }
}
